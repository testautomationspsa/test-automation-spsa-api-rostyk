package com.epam.services.slsa;

import com.epam.model.AllComment;
import com.epam.model.Comment;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class CommentServiceTest {

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "coachIdWithComments")
    public void testGetAllCommentsByCorrectCoachId(String id) {
        List<Comment> comments = getAllComments(id);
        comments.forEach(ValidationManager::verifyComment);
    }

    private List<Comment> getAllComments(String coachId) {
        return new CommentService().getAllCommentsByCoachId(coachId, "1", "100")
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(AllComment.class).getEntities();
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetAllCommentsByIncorrectCoachId(String id) {
        new CommentService().getAllCommentsByCoachId(id, "1", "11").statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "comments")
    public void testAddEditAndDeleteCommentByUserId(String coachId, Comment newComment, Comment commentToEdit) {
        Comment createdComment = addComment(coachId, newComment);
        editComment(coachId, "" + createdComment.getId(), commentToEdit);
        deleteCommentByUserId(coachId, "" + createdComment.getUserId());
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "comments")
    public void testAddEditAndDeleteCommentByCommentId(String coachId, Comment newComment, Comment commentToEdit) {
        Comment createdComment = addComment(coachId, newComment);
        editComment(coachId, "" + createdComment.getId(), commentToEdit);
        deleteCommentByCommentId(coachId, "" + createdComment.getId());
    }

    private Comment addComment(String coachId, Comment newComment) {
        Comment createdComment = new CommentService().addNewComment(coachId, newComment)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Comment.class);
        assertEquals(createdComment, newComment, "Comment was not added correctly!");
        assertTrue(getAllComments(coachId).stream().anyMatch(comment1 -> comment1.equals(createdComment)),
                "Comment was not added correctly!");
        return createdComment;
    }

    private void editComment(String coachId, String commentId, Comment commentToEdit) {
        Comment editedComment = new CommentService()
                .editComment(coachId, commentId, commentToEdit)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Comment.class);
        assertEquals(editedComment, commentToEdit, "Comment was not edited correctly!");
    }

    private void deleteCommentByUserId(String coachId, String userId) {
        new CommentService().deleteCommentByUserId(userId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllComments(coachId).stream().noneMatch(comment -> userId.equals(comment.getId() + "")),
                "Comment with userId = " + userId + " and coachId = " + coachId + " was not deleted!");
    }

    private void deleteCommentByCommentId(String coachId, String commentId) {
        new CommentService().deleteCommentByCommentId(coachId, commentId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllComments(coachId).stream().noneMatch(comment -> commentId.equals(comment.getId() + "")),
                "Comment with id = " + commentId + " and coachId = " + coachId + " was not deleted!");
    }
}