package com.epam.services.slsa;

import com.epam.model.Coach;
import com.epam.model.Form;
import com.epam.model.Role;
import com.epam.model.User;
import com.epam.services.spsa.FormService;
import com.epam.services.spsa.FormServiceTest;
import com.epam.services.spsa.UserService;
import com.epam.services.spsa.UserServiceTest;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static com.epam.utils.DataFactory.getFormList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CoachServiceTest {

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testGetAllCoachesByCorrectLocationId(String locationId) {
        List<Coach> coaches = getAllCoachesByLocationId(locationId);
        coaches.forEach(ValidationManager::verifyCoach);
    }

    public List<Coach> getAllCoachesByLocationId(String id) {
        return Arrays.asList(new CoachService().getAllCoachesByLocationId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Coach[].class));
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetAllCoachesByIncorrectLocationId(String locationId) {
        assertTrue(getAllCoachesByLocationId(locationId).isEmpty(),
                "This list should be empty but he isn`t!");
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationAndCoachId")
    public void testGetCoachByCorrectId(String locationId, String coachId) {
        Coach coach = getCoach(locationId, coachId);
        ValidationManager.verifyCoach(coach);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectLocationAndCoachId")
    public void testGetCoachByIncorrectId(String locationId, String coachId) {
        new CoachService().getCoachById(locationId, coachId).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "coaches")
    public void testAddAndEditAndDeleteCoach(User user, String locationId, Coach newCoach, Coach coachToEdit,
                                             String newLocation) {
        int userId = new UserServiceTest().createUser(user);
        setCoachRoleToTheUser(""+userId);
        newCoach.setUserId(userId);
        coachToEdit.setUserId(userId);
        Coach coach = addNewCoach(newCoach, locationId);
        assertEquals(coach, newCoach, "Coach was not added correctly!");
        assertEquals(getCoachByUserId("" + userId), newCoach, "User was not added correctly!");
        editCoach(locationId, "" + coach.getId(), coachToEdit);
        setNewLocationToTheCoach(""+coach.getId(), newLocation);
        deleteCoachLinkById("" + userId, "" + coach.getLinks().get(0).getId());
        deleteCoach(locationId, coach.getId());
        new UserServiceTest().deleteUserById(""+userId);
    }

    private void setCoachRoleToTheUser(String userId) {
        new UserServiceTest().setUserRole(userId, new Role(2,"COACH"));
    }

    private void setNewLocationToTheCoach(String coachId, String locationId) {
        new CoachService().setNewCoachLocation(coachId, locationId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllCoachesByLocationId(locationId).stream()
                .anyMatch(coach -> coachId.equals(coach.getId() + "")), "New location was not set!");
    }

    private Coach getCoach(String locationId, String coachId) {
        return new CoachService().getCoachById(locationId, coachId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Coach.class);
    }

    private Coach getCoachByUserId(String userId) {
        return new CoachService().getCoachByUserId(userId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Coach.class);
    }

    private Coach addNewCoach(Coach newCoach, String locationId) {
        return new CoachService().addCoach(locationId, newCoach)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Coach.class);
    }

    public void editCoach(String locationId, String coachId, Coach coachToEdit) {
        Coach editedCoach = new CoachService().editCoach(locationId, coachId, coachToEdit)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Coach.class);
        assertEquals(editedCoach, coachToEdit, "Coach was not edited!");
        assertEquals(getCoach(locationId, coachId), coachToEdit, "Coach was not edited!");
    }

    public void deleteCoach(String locationId, int coachId) {
        new CoachService().deleteCoach("" + coachId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllCoachesByLocationId(locationId).stream()
                .noneMatch(coach -> coach.getId() == coachId), "Coach with" + coachId + " was not deleted!");
    }

    public void deleteCoachLinkById(String userId, String linkId) {
        new CoachService().deleteLinkById(linkId).statusCode(HttpStatus.SC_OK);
        assertTrue(getCoachByUserId(userId).getLinks().stream().noneMatch(link -> linkId.equals("" + link.getId())),
                "Link with id = " + linkId + " was not deleted!");
    }
}