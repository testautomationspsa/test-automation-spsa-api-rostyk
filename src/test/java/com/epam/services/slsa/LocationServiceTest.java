package com.epam.services.slsa;

import com.epam.model.Location;
import com.epam.model.Photo;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static com.epam.utils.DataFactory.*;
import static com.epam.utils.DataFactory.getImageUrlToEdit;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.*;

public class LocationServiceTest {

    @Test
    public void testGetAllLocations() {
        List<Location> locations = Arrays.asList(new LocationService().getAllLocations()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location[].class));
        locations.forEach(ValidationManager::verifyLocation);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testGetLocationByCorrectId(String id) {
        Location location = getLocationById(id);
        ValidationManager.verifyLocation(location);
    }

    private Location getLocationById(String id) {
        return new LocationService().getLocationById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location.class);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetLocationByIncorrectId(String id) {
        new LocationService().getLocationById(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "locations")
    public void testAddEditAndDeleteLocation(Location newLocation, Location locationToEdit) throws FileNotFoundException {
        Location createdLocation = new LocationService().addLocation(newLocation)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Location.class);
        assertEquals(createdLocation, newLocation);
        assertEquals(getLocationById(createdLocation.getId() + ""), newLocation);
        new LocationScheduleServiceTest().addAndEditAndDeleteLocationSchedule(createdLocation.getId() + "");
        editLocation("" + createdLocation.getId(), locationToEdit);
        deleteLocation("" + createdLocation.getId());
    }

    public void editLocation(String id, Location location) {
        Location editedLocation = new LocationService().editLocation(id, location)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location.class);
        assertEquals(editedLocation, location);
    }

    public void deleteLocation(String id) {
        new LocationService().deleteLocation(id).statusCode(HttpStatus.SC_OK);
        new LocationService().getLocationById(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testAddEditAndDeleteLocationImage(String locationId) {
        List<Photo> photos = new LocationService().addLocationImage(getImage(), locationId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Location.class).getPhotos();
        String newPhotoId = "" + photos.get(photos.size() - 1).getId();
        editLocationImage(locationId, newPhotoId);
        deleteLocationImage(locationId, newPhotoId);
    }

    public void editLocationImage(String locationId, String imageId) {
        new LocationService().editLocationImage(getImageToEdit(), locationId, imageId)
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testAddEditAndDeleteLocationImageUrl(String locationId) {
        List<Photo> photos = new LocationService().addLocationImageUrl(getImageUrl(), locationId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location.class).getPhotos();
        assertTrue(photos.stream().anyMatch(photo -> photo.getUrl().equals(getImageUrl())),
                "Photo with locationId = " + locationId + " was not added!");
        String newPhotoId = "" + photos.get(photos.size() - 1).getId();
        editLocationImageUrl(locationId, newPhotoId);
        deleteLocationImage(locationId, newPhotoId);
    }

    public void editLocationImageUrl(String locationId, String imageId) {
        List<Photo> photos = new LocationService().editLocationImageUrl(getImageUrlToEdit(), locationId, imageId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location.class).getPhotos();
        assertTrue(photos.stream().filter(photo -> imageId.equals("" + photo.getId()))
                        .allMatch(photo -> photo.getUrl().equals(getImageUrlToEdit())),
                "Photo with id = " + imageId + " and locationId = " + locationId + " was not updated!");
    }

    public void deleteLocationImage(String locationId, String imageId) {
        new LocationService().deleteLocationImage(locationId, imageId).statusCode(HttpStatus.SC_OK);
        List<Photo> photos = getLocationById(locationId).getPhotos();
        assertTrue(photos.stream().noneMatch(photo -> ("" + photo.getId()).equalsIgnoreCase(imageId)),
                "Photo with id = " + imageId + " and locationId = " + locationId + " was not deleted!");
    }
}