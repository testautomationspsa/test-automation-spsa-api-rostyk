package com.epam.services.slsa;

import com.epam.model.LocationType;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.*;

public class LocationTypeServiceTest {

    @Test
    public void testGetAllLocationTypes() {
        List<LocationType> locationTypes = Arrays.asList(new LocationTypeService().getAllLocationTypes()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationType[].class));
        locationTypes.forEach(ValidationManager::verifyLocationType);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationTypeId")
    public void testGetLocationTypeByCorrectId(String id) {
        LocationType locationType = new LocationTypeService().getLocationTypeById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationType.class);
        ValidationManager.verifyLocationType(locationType);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetLocationTypeByIncorrectId(String id) {
        new LocationTypeService().getLocationTypeById(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}