package com.epam.services.slsa;

import com.epam.model.LocationAddress;
import com.epam.model.UserAddress;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.equalTo;

public class LocationAddressServiceTest {

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctCoordinates")
    public void testGetAddressByCorrectCoordinates(String latitude, String longitude) {
        LocationAddress address = new AddressService().getAddressByCoordinates(latitude, longitude)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationAddress.class);
        ValidationManager.verifyAddress(address);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectCoordinates")
    public void testGetAddressByIncorrectCoordinates(String latitude, String longitude) {
        new AddressService().getAddressByCoordinates(latitude, longitude)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testGetAddressByCorrectLocationId(String id) {
        LocationAddress address = new AddressService().getAddressByLocationId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationAddress.class);
        ValidationManager.verifyAddress(address);
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetAddressByIncorrectLocationId(String id) {
        new AddressService().getAddressByLocationId(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}