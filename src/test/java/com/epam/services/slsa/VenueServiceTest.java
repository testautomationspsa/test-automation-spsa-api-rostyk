package com.epam.services.slsa;

import com.epam.model.Location;
import com.epam.model.LocationCriteria;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class VenueServiceTest {

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "criteriaToFind")
    public void testViewMatchedListOfVenues(LocationCriteria criteria) {
        List<Location> locationCriteria = Arrays.asList(new VenueService()
                .viewMatchedListOfVenues(criteria)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location[].class));
        locationCriteria.forEach(ValidationManager::verifyLocation);
        assertFalse(locationCriteria.isEmpty(), "List should be filled!");
    }
}