package com.epam.services.slsa;

import com.epam.model.LocationSchedule;
import com.epam.utils.SLSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static com.epam.utils.DataFactory.getLocationScheduleToEdit;
import static com.epam.utils.DataFactory.getNewLocationSchedule;
import static org.testng.Assert.*;

public class LocationScheduleServiceTest {

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "correctLocationId")
    public void testGetLocationScheduleByCorrectId(String locationId) {
        List<LocationSchedule> schedules = getLocationScheduleById(locationId);
        schedules.forEach(ValidationManager::verifyLocationSchedule);
    }

    private List<LocationSchedule> getLocationScheduleById(String locationId) {
        return Arrays.asList(new LocationScheduleService()
                .getLocationScheduleById(locationId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationSchedule[].class));
    }

    @Test(dataProviderClass = SLSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetLocationScheduleByIncorrectId(String locationId) {
        new LocationScheduleService()
                .getLocationScheduleById(locationId)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    public void addAndEditAndDeleteLocationSchedule(String locationId) throws FileNotFoundException {
        List<LocationSchedule> postedSchedule = Arrays.asList(new LocationScheduleService()
                .addLocationSchedule(locationId, getNewLocationSchedule())
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(LocationSchedule[].class));
        getNewLocationSchedule().forEach(schedule -> assertTrue(postedSchedule.contains(schedule),
                "Location shedules was not added correctly!"));
        editLocationSchedule(locationId);
        postedSchedule.forEach(schedule -> deleteLocationSchedule(schedule.getId() + "", locationId));
        assertTrue(getLocationScheduleById(locationId).isEmpty(),
                "Schedules is not empty, but they should be so!");
    }

    private void editLocationSchedule(String locationId) throws FileNotFoundException {
        List<LocationSchedule> editedSchedule = Arrays.asList(new LocationScheduleService()
                .editLocationSchedule(locationId, getLocationScheduleToEdit())
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(LocationSchedule[].class));
        assertEquals(editedSchedule, getLocationScheduleToEdit());
    }

    private void deleteLocationSchedule(String scheduleId, String locationId) {
        new LocationScheduleService().deleteLocationSchedule(scheduleId).statusCode(HttpStatus.SC_OK);
    }
}