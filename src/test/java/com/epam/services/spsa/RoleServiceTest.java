package com.epam.services.spsa;

import com.epam.model.Role;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class RoleServiceTest {

    @Test
    public void testGetAllRoles() {
        List<Role> roles = Arrays.asList(new RoleService().getAllRoles()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Role[].class));
        roles.forEach(ValidationManager::verifyRole);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctRoleId")
    public void testGetRoleByCorrectId(String id) {
        Role role = new RoleService().getRoleById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Role.class);
        ValidationManager.verifyRole(role);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetRoleByIncorrectId(String id) {
        new RoleService().getRoleById(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }
}