package com.epam.services.spsa;

import com.epam.model.Statistics;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static com.epam.utils.ValidationManager.verifyStatistics;
import static org.testng.Assert.*;

public class UserStatisticsServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUsersId")
    public void testGetAllUserStatistics(String userId) {
        List<Statistics> statistics = getAllUserStatistics(userId);
        statistics.forEach(ValidationManager::verifyStatistics);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUsersId")
    public void testGetUserLastStatistics(String userId) {
        Statistics statistics = getUserLastStatistics(userId);
        verifyStatistics(statistics);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetAllUserStatisticsByIncorrectId(String userId) {
        new UserStatisticsService().getAllUserStatistics(userId).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetUserLastStatisticsByIncorrectI(String userId) {
        new UserStatisticsService().getUserLastStatistics(userId).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    public List<Statistics> getAllUserStatistics(String userId){
        return Arrays.asList(new UserStatisticsService()
                .getAllUserStatistics(userId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Statistics[].class));
    }

    public Statistics getUserLastStatistics(String userId){
        return new UserStatisticsService()
                .getUserLastStatistics(userId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Statistics.class);
    }

    public void testSaveAndEditStatistics(String userId, Statistics newStatistics, Statistics statisticsToEdit){
        saveStatistics(userId, newStatistics);
//        getStatisticsByParam()
        editUserLastStatistics(userId, statisticsToEdit);
    }

    private void getStatisticsByParam(String userId, Statistics param){
        Statistics statistics = new UserStatisticsService().getUserStatisticsByParam(userId, param)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Statistics.class);

    }

    private void saveStatistics(String userId, Statistics statistics) {
        Statistics createdStatistics = new UserStatisticsService()
                .saveStatistics(userId, statistics)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Statistics.class);
        assertEquals(createdStatistics, statistics, "Statistics was not added correctly!");
        assertEquals(getUserLastStatistics(userId), statistics, "Statistics was not added correctly!");
    }

    private void editUserLastStatistics(String userId, Statistics statistics) {
        Statistics editedStatistics = new UserStatisticsService()
                .editUserLastStatistics(userId, statistics)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Statistics.class);
        assertEquals(editedStatistics, statistics, "Statistics was not edited correctly!");
        assertEquals(getUserLastStatistics(userId), statistics, "Statistics was not edited correctly!");
    }

}