package com.epam.services.spsa;

import com.epam.listener.TestListener;
import com.epam.model.UserAddress;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.assertNotEquals;

@Listeners({TestListener.class})
public class UserAddressServiceTest {

    @Test
    public void getAllAddressesTest() {
        List<UserAddress> userAddresses = Arrays.asList(new AddressService().getAllAddresses()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(UserAddress[].class));
        assertNotEquals(userAddresses, Collections.EMPTY_LIST);
        userAddresses.forEach(ValidationManager::verifyAddress);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUsersId")
    public void getAddressByCorrectUsersIdTest(String id) {
        UserAddress userAddress = new AddressService().getAddressByUsersId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(UserAddress.class);
        ValidationManager.verifyAddress(userAddress);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void getAddressByIncorrectUsersIdTest(String id) {
        new AddressService().getAddressByUsersId(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectCoordinates")
    public void getAddressByIncorrectCoordinatesTest(String latitude, String longitude) {
        new AddressService().getAddressByCoordinates(latitude, longitude)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctCoordinates")
    public void getAddressByCorrectCoordinatesTest(String latitude, String longitude) {
        List<UserAddress> userAddresses = Arrays.asList(new AddressService().getAddressByCoordinates(latitude, longitude)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(UserAddress[].class));
        userAddresses.forEach(ValidationManager::verifyAddress);
    }


}
