package com.epam.services.spsa;

import com.epam.model.Article;
import com.epam.model.Tag;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TagServiceTest {

    @Test
    public void testGetAllTags() {
        List<Tag> tags = getAllTagList();
        tags.forEach(ValidationManager::verifyTag);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctTagId")
    public void testGetTagByCorrectId(String id) {
        Tag tag = getTagByCorrectId(id);
        ValidationManager.verifyTag(tag);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetTagByIncorrectId(String id) {
        new TagService().getTagById(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getCorrectTagsPagesAndSizes")
    public void testGetArticlesByCorrectTag(String pageNumber, String sizeNumber, String tag) {
        List<Article> articles = Arrays.asList(new TagService()
                .getArticlesByTag(pageNumber, sizeNumber, tag)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Article[].class));
        articles.forEach(ValidationManager::verifyArticle);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getIncorrectTagsPagesAndSizes")
    public void testGetArticlesByIncorrectTag(String pageNumber, String sizeNumber, String tag) {
        new TagService().getArticlesByTag(pageNumber, sizeNumber, tag).statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "tags")
    public void testAddAndDeleteTag(String tag) {
        Tag toPost = new Tag(0, tag);
        Tag postedTag = new TagService().addTag(toPost)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Tag.class);
        assertEquals(postedTag, toPost, "Tag was not added");
        assertEquals(getTagByCorrectId(""+postedTag.getId()), toPost, "Tag was not added");
        deleteTag("" + postedTag.getId());
    }

    private void deleteTag(String tagId) {
        new TagService().deleteTag(tagId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllTagList().stream().noneMatch(tag -> tagId.equals("" + tag.getId())),
                "Tag with id = " + tagId + " was not deleted!");
    }

    private List<Tag> getAllTagList() {
        return Arrays.asList(new TagService().getAllTags()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Tag[].class));
    }

    private Tag getTagByCorrectId(String id) {
        return new TagService().getTagById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Tag.class);
    }
}