package com.epam.services.spsa;

import com.epam.model.Event;
import com.epam.model.User;
import com.epam.utils.SPSADataProviderManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static com.epam.utils.Constants.DEFAULT_PASSWORD;
import static org.testng.Assert.*;

public class EventServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "events")
    public void testAddEditAndDeleteEvent(Event newEvent, Event eventToEdit) {
        Event createdEvent = addEvent(newEvent);
        String newEventId = createdEvent.getId() + "";
        assertEquals(createdEvent, newEvent, "Event was not added correctly!");
        assertEquals(getEventByEventId(newEventId), newEvent, "Event was not added correctly!");
        editEvent(eventToEdit, newEventId);
        deleteEvent(newEventId, getToken(createdEvent.getOwner_id()));
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectEvents")
    public void testAddIncorrectEvents(Event event){
        new EventService().addEvent(event).statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetEventByIncorrectUserId(String id){
        new EventService().getEventByUserId(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetEventByIncorrectEventId(String id){
        new EventService().getEventByEventId(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    private String getToken(int ownerId) {
        User owner = new UserServiceTest().getUserByCorrectId("" + ownerId);
        return new AuthenticationServiceTest().getToken(owner.getEmail(), DEFAULT_PASSWORD);
    }

    private Event addEvent(Event event) {
        return new EventService().addEvent(event)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Event.class);
    }

    private void editEvent(Event event, String id) {
        Event editedEvent = new EventService().editEvent(event, id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Event.class);
        assertEquals(editedEvent, event, "Event was not edited correctly!");
        assertEquals(getEventByEventId(id), event, "Event was not edited correctly!");
        assertTrue(getEventsByUserId("" + editedEvent.getOwner_id()).contains(editedEvent),
                "Event was not edited correctly!");
    }

    private Event getEventByEventId(String id) {
        return new EventService().getEventByEventId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Event.class);
    }

    private List<Event> getEventsByUserId(String id) {
        return Arrays.asList(new EventService().getEventByUserId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Event[].class));
    }

    private List<Event> getAllEvents() {
        return Arrays.asList(new EventService().getAllEvents()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Event[].class));
    }

    private void deleteEvent(String id, String token) {
        new EventService().deleteEvent(id, token).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllEvents().stream().noneMatch(event -> id.equals(event.getId() + "")),
                "Event with id = " + id + " was not deleted!");
    }

}