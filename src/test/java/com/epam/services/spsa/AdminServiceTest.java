package com.epam.services.spsa;

import com.epam.model.Location;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class AdminServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctAdminId", enabled = false)
    public void testGetAllLocationsByCorrectAdminId(String id) {
        List<Location> locations = Arrays.asList(new AdminService().getAllLocationsByAdminId(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Location[].class));
        locations.forEach(ValidationManager::verifyLocation);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId",enabled = false)
    public void testGetAllLocationsByIncorrectAdminId(String id) {
        new AdminService().getAllLocationsByAdminId(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}