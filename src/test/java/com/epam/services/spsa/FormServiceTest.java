package com.epam.services.spsa;

import com.epam.model.AllForm;
import com.epam.model.Form;
import com.epam.model.User;
import com.epam.utils.DataFactory;
import com.epam.utils.SPSADataProviderManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class FormServiceTest {


    public void testAddingAndCancelingForm(Form form, String email, String password, String page, String size) {
        saveAndDeleteForm(form,email,password,page,size);
        saveAndApproveForm(form,email,password,page,size);
    }

    private void saveAndApproveForm(Form form, String email, String password, String page, String size) {
        Form savedForm = getSavedForm(form, getToken(email, password));
        assertEquals(getFormByCorrectId(savedForm.getId()), form, "Form was not saved correctly!");
        assertTrue(getForms(page, size).stream().anyMatch(form1 -> form1.equals(form)), "Form was not saved!");
        approveForm(savedForm.getId(), email, form.getRole().getName());
        logOut();
    }

    private void saveAndDeleteForm(Form form, String email, String password, String page, String size) {
        Form savedForm = getSavedForm(form, getToken(email, password));
        assertEquals(getFormByCorrectId(savedForm.getId()), form, "Form was not saved correctly!");
        assertTrue(getForms(page, size).stream().anyMatch(form1 -> form1.equals(form)), "Form was not saved!");
        deleteForm("" + savedForm.getId(), getToken(email, password));
        logOut();
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "forms")
    public void testSaveFormUnauthorized(Form form, String email, String password, String page, String size) {
        new FormService().saveForm(form, "").statusCode(HttpStatus.SC_NOT_FOUND);
    }

    public Form getSavedForm(Form form, String token) {
        return new FormService().saveForm(form, token)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Form.class);
    }

    public String getToken(String login, String password) {
        return new AuthenticationService().logIn(login, password)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .cookie("AJWT");
    }

    private void logOut(){
        new AuthenticationService().logOut().statusCode(HttpStatus.SC_OK);
    }

    private Form getFormByCorrectId(int id) {
        return new FormService().getFormById("" + id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Form.class);
    }

    public void approveForm(int id, String email, String role) {
        new FormService().approveForm("" + id).statusCode(HttpStatus.SC_OK);
        List<User> users = Arrays.asList(new UserService().getAllUsers()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(User[].class));
        User user = users.stream().filter(user1 -> user1.getEmail().equals(email)).findFirst().get();
        assertTrue(user.getRoles().stream().anyMatch(role1 -> role1.getName().equalsIgnoreCase(role)),
                "Role was not added!");
    }

    private List<Form> getForms(String page, String size) {
        return new FormService().getForms(page, size)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(AllForm.class).getEntities();
    }

    private void deleteForm(String id, String token) {
        new FormService().deleteForm(id, token).statusCode(HttpStatus.SC_OK);
        new FormService().getFormById(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }
}