package com.epam.services.spsa;

import com.epam.model.Criteria;
import com.epam.model.PartnerShell;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class PartnerServiceTest {

    @Test
    public void testGetAllCriteria() {
        List<Criteria> criteria = Arrays.asList(new PartnerService().getAllCriteria()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Criteria[].class));
        criteria.forEach(ValidationManager::verifyCriteria);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUserIdForCriteria")
    public void testGetAllCriteriaByCorrectUserId(String userId) {
        getCriteriaList(userId).forEach(ValidationManager::verifyCriteria);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetAllCriteriaByIncorrectUserId(String userId) {
        new PartnerService().getAllCriteriaByUserId(userId)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "criteriaAndIncorrectUserIdAndPageAndSize")
    public void testGetPartners(String userId, Criteria criteria, String pageNumber, String pageSize) {
        PartnerShell partners =new PartnerService().getPartners(userId, criteria, pageNumber, pageSize)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(PartnerShell.class);
        partners.getEntities().forEach(ValidationManager::verifyCriteriaUser);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "criteriaAndIncorrectUserId", priority = 0)
    public void testSaveCriteriaWithIncorrectUserId(String userId, Criteria criteria) {
        new PartnerService().saveCriteria(userId, criteria)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "criteriaAndCorrectUserId")
    public void testSaveAndDeleteCriteriaWithCorrectUserId(String userId, Criteria criteria) {
        int postedCriteriaId = new PartnerService().saveCriteria(userId, criteria)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .jsonPath()
                .get("id");
        assertTrue(getCriteriaList(userId).stream().anyMatch(criteria1 -> criteria1.getId() == postedCriteriaId)
                , "Criteria was not created!");
        deleteCriteria(userId, postedCriteriaId);
    }

    public void deleteCriteria(String userId, int criteriaId) {
        new PartnerService().deleteCriteria(userId, "" + criteriaId)
                .statusCode(HttpStatus.SC_OK);
        assertTrue(getCriteriaList(userId).stream()
                        .noneMatch(criteria1 -> criteriaId == criteria1.getId()),
                "Criteria with id = " + userId + " was not deleted!");
    }

    private List<Criteria> getCriteriaList(String usersId) {
        return Arrays.asList(new PartnerService().getAllCriteriaByUserId(usersId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Criteria[].class));
    }
}