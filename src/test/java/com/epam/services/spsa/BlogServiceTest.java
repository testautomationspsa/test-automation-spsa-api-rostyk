package com.epam.services.spsa;

import com.epam.model.AllArticle;
import com.epam.model.Article;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.List;

import static com.epam.utils.DataFactory.*;
import static org.testng.Assert.assertEquals;

public class BlogServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getCorrectPagesAndSizes")
    public void testGetArticlesByCorrectPageAndSize(String page, String size) {
        List<Article> articles = new BlogService().getArticles(page, size)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(AllArticle.class).getEntities();
        articles.forEach(ValidationManager::verifyArticle);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getCorrectArticleId")
    public void testGetArticleByCorrectId(String articleId) {
        Article article = getArticleByCorrectId(articleId);
        ValidationManager.verifyArticle(article);
    }

    private Article getArticleByCorrectId(String articleId) {
        return new BlogService().getArticleById(articleId)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Article.class);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "articles")
    public void testAddEditAndDeleteArticle(Article newArticle, Article articleToEdit) {
        Article postedArticle = addArticle(newArticle);
        assertEquals(postedArticle, newArticle, "Article was not added");
        assertEquals(getArticleByCorrectId("" + postedArticle.getId()), newArticle,
                "Article was not added");
        editArticle("" + postedArticle.getId(), articleToEdit);
        deleteArticle("" + postedArticle.getId());
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "articles")
    public void testAddEditAndDeleteArticleWithImage(Article newArticle, Article articleToEdit) {
        Article postedArticle = addArticle(newArticle);
        String newArticleId = "" + postedArticle.getId();
        assertEquals(postedArticle, newArticle, "Article was not added");
        assertEquals(getArticleByCorrectId(newArticleId), newArticle,
                "Article was not added");
        editArticle(newArticleId, articleToEdit);
        addAndEditAndDeleteArticleImage(newArticleId);
        addEditAndDeleteArticleImageUrl(newArticleId);
        deleteArticle("" + postedArticle.getId());
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testAddArticleImageByIncorrectId(String id) {
        new BlogService().addArticleImage(getImage(), id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getCorrectArticleId", enabled = false)
    public void testAddArticleImageUrlByIncorrectUrl(String id) {
        new BlogService().addArticleImageUrl("image-url", id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testEditImageByIncorrectId(String id) {
        new BlogService().editArticleImage(getImageToEdit(), id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetArticleByIncorrectId(String articleId) {
        getArticleByIncorrectId(articleId);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "getIncorrectPagesAndSizes")
    public void testGetArticlesByIncorrectPageAndSize(String page, String size) {
        new BlogService().getArticles(page, size).statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    private void getArticleByIncorrectId(String articleId) {
        new BlogService().getArticleById(articleId).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    private Article addArticle(Article newArticle){
        return new BlogService().addArticle(newArticle)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(Article.class);
    }

    public void addAndEditAndDeleteArticleImage(String articleId) {
        new BlogService().addArticleImage(getImage(), articleId).statusCode(HttpStatus.SC_CREATED);
        new BlogService().editArticleImage(getImageToEdit(), articleId).statusCode(HttpStatus.SC_OK);
        deleteArticleImage(articleId);
    }

    private void addEditAndDeleteArticleImageUrl(String id) {
        new BlogService().addArticleImageUrl(getImageUrl(), id).statusCode(HttpStatus.SC_CREATED);
        new BlogService().editArticleImageUrl(getImageUrlToEdit(), id).statusCode(HttpStatus.SC_OK);
        deleteArticleImage(id);
    }

    private void deleteArticle(String articleId) {
        new BlogService().deleteArticle(articleId).statusCode(HttpStatus.SC_OK);
        getArticleByIncorrectId(articleId);
    }

    private void deleteArticleImage(String articleId) {
        new BlogService().deleteArticlesImage(articleId).statusCode(HttpStatus.SC_OK);
    }

    private void editArticle(String id, Article article) {
        Article editedArticle = new BlogService().editArticle(article, id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Article.class);
        assertEquals(editedArticle, article, "Article was not edited");
        assertEquals(getArticleByCorrectId(id), article, "Article was not edited");
    }
}