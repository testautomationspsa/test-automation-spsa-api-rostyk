package com.epam.services.spsa;

import com.epam.model.User;
import com.epam.utils.SPSADataProviderManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AuthenticationServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctEmailAndPassword")
    public void testSuccessfulLogInAndLogOut(String email, String password) {
        String token = getToken(email, password);
        String currentUsersEmail = getCurrentSignInUser(token).getEmail();
        assertEquals(currentUsersEmail, email, "Sign in with another email!");
        logOut();
    }

    private User getCurrentSignInUser(String token){
        return new UserService().getCurrentSignInUser(token)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(User.class);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctEmailAndPassword")
    public void goOnlineAndOffline(String email, String password){
        String token = getToken(email, password);
        new UserService().goOnline(token).statusCode(HttpStatus.SC_OK);
        assertTrue(getCurrentSignInUser(token).isOnline(), "User is not online but should be!");
        new UserService().goOffline(token).statusCode(HttpStatus.SC_OK);
        assertFalse(getCurrentSignInUser(token).isOnline(), "User is not offline but should be!");
    }

    public String getToken(String login, String password) {
        return new AuthenticationService().logIn(login, password)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .cookie("AJWT");
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectEmailAndPassword")
    public void testLogInFailed(String email, String password) {
        new AuthenticationService().logIn(email, password).statusCode(HttpStatus.SC_BAD_REQUEST);
        new UserService().getCurrentSignInUser("token").statusCode(HttpStatus.SC_NOT_FOUND);
    }

    private void logOut() {
        new AuthenticationService().logOut().statusCode(HttpStatus.SC_OK);
    }
}