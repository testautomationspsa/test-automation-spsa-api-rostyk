package com.epam.services.spsa;

import com.epam.model.Form;
import com.epam.model.Role;
import com.epam.model.Statistics;
import com.epam.model.User;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static com.epam.utils.DataFactory.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.*;

public class UserServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "users")
    public void testAddEditAndDeleteUserWithImage(User newUser, User userToEdit, Statistics newStatistics,
                                                  Statistics statisticsToEdit, Form form, String page, String size) {
        String newUserId = "" + createUser(newUser);
        User postedUser = getUserByCorrectId(newUserId);
        assertEquals(postedUser, newUser, "User was not created correctly!");
        new UserStatisticsServiceTest().testSaveAndEditStatistics(newUserId, newStatistics, statisticsToEdit);
        new FormServiceTest().testAddingAndCancelingForm(form, newUser.getEmail(), newUser.getPassword(), page, size);
        editUser(newUserId, userToEdit);
        addAndEditAndDeleteUserImage(newUserId);
        deleteUserById(newUserId);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "users")
    public void testAddEditAndDeleteUserWithImageUrl(User newUser, User userToEdit, Statistics newStatistics,
                                                     Statistics statisticsToEdit, Form form, String page, String size) {
        String newUserId = "" + createUser(newUser);
        User postedUser = getUserByCorrectId(newUserId);
        assertEquals(postedUser, newUser, "User was not created correctly!");
        new UserStatisticsServiceTest().testSaveAndEditStatistics(newUserId, newStatistics, statisticsToEdit);
        new FormServiceTest().testAddingAndCancelingForm(form, newUser.getEmail(), newUser.getPassword(), page, size);
        editUser(newUserId, userToEdit);
        addAndEditAndDeleteUserImageUrlByCorrectId(getImageUrl(), getImageUrlToEdit(), newUserId);
        deleteUserById(newUserId);
    }

    @Test
    public void testAddUserWithIncorrectEmail() {
        User newUser = new User();
        newUser.setEmail("1234");
        new UserService().addUser(newUser).statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testGetAllUsers() {
        List<User> users = getAllUsers();
        users.forEach(ValidationManager::verifyUser);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUsersId")
    public void testGetUserByCorrectId(String usersId) {
        User user = getUserByCorrectId(usersId);
        ValidationManager.verifyUser(user);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetUserByIncorrectId(String usersId) {
        new UserService().getUserById(usersId).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testAddUserImageByIncorrectId(String id) {
        new UserService().addUserImage(getImage(), id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctUsersId")
    public void testAddUserImageUrlByIncorrectUrl(String id) {
        new UserService().addUserImageUrl("image-url", id).statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testEditUserImageByIncorrectId(String id) {
        new BlogService().editArticleImage(getImageToEdit(), id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    public int createUser(User newUser) {
        return new UserService().addUser(newUser)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .jsonPath()
                .get("id");
    }

    private void editUser(String userId, User user) {
        User editedUser = new UserService().editUserById(userId, user)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(User.class);
        assertEquals(editedUser, user, "User was not edited!");
        assertEquals(getUserByCorrectId(userId), user, "User was not edited!");
    }

    public User getUserByCorrectId(String id) {
        return new UserService().getUserById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(User.class);
    }

    private List<User> getAllUsers() {
        return Arrays.asList(new UserService().getAllUsers()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(User[].class));
    }

    public void deleteUserById(String usersId) {
        new UserService().deleteUserById(usersId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllUsers().stream().noneMatch(user -> usersId.equals("" + user.getId())),
                "User with id = " + usersId + " was not deleted!");
    }

    private void addAndEditAndDeleteUserImage(String userId) {
        new UserService().addUserImage(getImage(), userId).statusCode(HttpStatus.SC_CREATED);
        new UserService().editUserImage(getImageToEdit(), userId).statusCode(HttpStatus.SC_OK);
        deleteUserImage(userId);
    }

    private void addAndEditAndDeleteUserImageUrlByCorrectId(String imageUrl, String ImageUrlToEdit, String id) {
        new UserService().addUserImageUrl(imageUrl, id).statusCode(HttpStatus.SC_CREATED)
                .body("photo", equalTo(imageUrl));
        new UserService().editUserImageUrl(ImageUrlToEdit, id).statusCode(HttpStatus.SC_OK)
                .body("photo", equalTo(ImageUrlToEdit));
        deleteUserImage(id);
    }

    private void deleteUserImage(String userId) {
        new UserService().deleteUsersImage(userId).statusCode(HttpStatus.SC_OK);
        assertNull(getUserByCorrectId(userId).getPhoto(), "User photo was not deleted!");
    }

    public void deleteUserRole(String id) {
        new UserService().deleteUsersRole(id, new Role(1, "USER")).statusCode(HttpStatus.SC_OK);
    }

    public void setUserRole(String userId, Role role) {
        new UserService().saveRole(role, userId).statusCode(HttpStatus.SC_OK);
        assertTrue(getAllUsers().stream().filter(user -> userId.equals("" + user.getId())).anyMatch(user ->
                user.getRoles().contains(role)), "Role was not added to the user with id = " + userId);
    }

}