package com.epam.services.spsa;

import com.epam.model.ArticleComment;
import com.epam.utils.SPSADataProviderManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static com.epam.utils.DataFactory.getImage;
import static com.epam.utils.DataFactory.getImageToEdit;
import static org.testng.Assert.*;

public class ArticleCommentServiceTest {

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctComments")
    public void testAddEditReplyAndDeleteComment(String blogId, String userId, String comment) {
        ArticleComment postedComment = addNewComment(blogId, userId, comment);
        String newCommentId = "" + postedComment.getId();
        assertEquals(incrementCommentRating(newCommentId), 1, "Rating was not incremented!");
        assertEquals(decrementCommentRating(newCommentId), 0, "Rating was not decremented!");
        editComment(newCommentId, comment.toUpperCase());
        replyComment(newCommentId, userId, comment.toUpperCase());
        deleteComment(newCommentId);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctComments")
    public void testAddEditReplyAndDeleteCommentWithImage(String blogId, String userId, String comment) {
        ArticleComment postedComment = addNewCommentWithImage(blogId, userId, comment);
        String newCommentId = "" + postedComment.getId();
        assertEquals(incrementCommentRating(newCommentId), 1, "Rating was not incremented!");
        assertEquals(decrementCommentRating(newCommentId), 0, "Rating was not decremented!");
        editCommentWithImage(newCommentId, comment.toUpperCase());
        replyCommentWithImage(newCommentId, userId, comment.toUpperCase());
        deleteComment(newCommentId);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void addNewCommentByIncorrectId(String blogId, String userId, String comment){
        new ArticleCommentService().addNewComment(blogId, userId, comment)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void addNewCommentWithImageByIncorrectId(String blogId, String userId, String comment){
        new ArticleCommentService().addNewCommentWithImage(blogId, userId, getImage(), comment)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void editCommentByIncorrectId(String commentId, String blogId, String comment){
        new ArticleCommentService().editComment(commentId, comment).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void editCommentWithImageByIncorrectId(String commentId, String blogId, String comment){
        new ArticleCommentService().editCommentWithImage(commentId, getImage(), comment)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void decrementCommentRatingByIncorrectId(String id){
        new ArticleCommentService().decrementRatingOfComment(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void incrementCommentRatingByIncorrectId(String id){
        new ArticleCommentService().incrementRatingOfComment(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void replyCommentByIncorrectId(String blogId, String userId, String comment){
        new ArticleCommentService().addNewReplyComment(blogId, userId, comment)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectComments")
    public void replyCommentWithImageByIncorrectId(String blogId, String userId, String comment){
        new ArticleCommentService().addNewReplyCommentWithImage(blogId, userId, getImage(), comment)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    private ArticleComment addNewComment(String blogId, String userId, String comment) {
        ArticleComment postedComment = new ArticleCommentService()
                .addNewComment(blogId, userId, comment)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(ArticleComment.class);
        checkEquality(postedComment.getContent(), comment);
        checkEquality("" + postedComment.getUser().getId(), userId);
        assertTrue(getAllComments().contains(postedComment), "Comment was not added!");
        return postedComment;
    }

    private ArticleComment addNewCommentWithImage(String blogId, String userId, String comment) {
        ArticleComment postedComment = new ArticleCommentService()
                .addNewCommentWithImage(blogId, userId, getImage(), comment)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(ArticleComment.class);
        checkEquality(postedComment.getContent(), comment);
        checkEquality("" + postedComment.getUser().getId(), userId);
        assertNotNull(postedComment.getImage(), "Photo was not added!");
        assertTrue(getAllComments().contains(postedComment), "Comment was not added!");
        return postedComment;
    }

    private void checkEquality(String first, String second) {
        assertEquals(first, second, "Comment was not added correctly!");
    }

    private void editComment(String commentId, String comment) {
        String editedComment = new ArticleCommentService()
                .editComment(commentId, comment)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .jsonPath()
                .get("content");
        assertEquals(editedComment, comment, "Comment was not edited!");
    }

    private void editCommentWithImage(String commentId, String comment) {
        ArticleComment editedComment = new ArticleCommentService()
                .editCommentWithImage(commentId, getImageToEdit(), comment)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(ArticleComment.class);
        assertEquals(editedComment.getContent(), comment, "Comment was not edited!");
        assertNotNull(editedComment.getImage(), "Photo was not edited!");
    }

    private int decrementCommentRating(String id) {
        return new ArticleCommentService().decrementRatingOfComment(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .jsonPath()
                .get("rating");
    }

    private int incrementCommentRating(String id) {
        return new ArticleCommentService().incrementRatingOfComment(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .jsonPath()
                .get("rating");
    }

    private void replyComment(String commentId, String userId, String comment) {
        ArticleComment reply = new ArticleCommentService()
                .addNewReplyComment(commentId, userId, comment)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(ArticleComment.class);
        assertTrue(reply.getContent().equals(comment) && userId.equals("" + reply.getUser().getId()),
                "Wrong reply added!");
        assertTrue(getCommentById(commentId).getComments().contains(reply),
                "This comment is not a reply to another");

    }

    private void replyCommentWithImage(String commentId, String userId, String comment) {
        ArticleComment reply = new ArticleCommentService()
                .addNewReplyCommentWithImage(commentId, userId, getImageToEdit(), comment)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(ArticleComment.class);
        assertTrue(reply.getContent().equals(comment) && userId.equals("" + reply.getUser().getId()),
                "Wrong reply added!");
        assertTrue(getCommentById(commentId).getComments().contains(reply),
                "This comment is not a reply to another");

    }

    private ArticleComment getCommentById(String id) {
        return new ArticleCommentService().getCommentById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(ArticleComment.class);
    }

    private List<ArticleComment> getAllComments() {
        return Arrays.asList(new ArticleCommentService()
                .getAllComments()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(ArticleComment[].class));
    }

    private void deleteComment(String id){
        new ArticleCommentService().deleteComment(id).statusCode(HttpStatus.SC_OK);
        new ArticleCommentService().getCommentById(id).statusCode(HttpStatus.SC_NOT_FOUND);
    }

}