package com.epam.services.spsa;

import com.epam.model.SportType;
import com.epam.services.spsa.SportTypeService;
import com.epam.utils.SPSADataProviderManager;
import com.epam.utils.ValidationManager;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.testng.Assert.assertNotEquals;

public class SportTypeServiceTest {

    @Test
    public void testGetAllSportTypes() {
        List<SportType> sportTypes = Arrays.asList(new SportTypeService().getAllSportTypes()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(SportType[].class));
        assertNotEquals(sportTypes, Collections.EMPTY_LIST);
        sportTypes.forEach(ValidationManager::verifySportType);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "correctSportTypeId")
    public void testGetSportTypeByCorrectId(String id) {
        SportType sportType = new SportTypeService().getSportTypeById(id)
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(SportType.class);
        ValidationManager.verifySportType(sportType);
    }

    @Test(dataProviderClass = SPSADataProviderManager.class, dataProvider = "incorrectId")
    public void testGetSportTypeByIncorrectId(String id) {
        new SportTypeService().getSportTypeById(id)
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}