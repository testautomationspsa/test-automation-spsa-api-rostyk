if exist "C:\D\EPAM\Internal Project\test-automation_remote\allure-report\" rmdir "C:\D\EPAM\Internal Project\test-automation_remote\allure-report" /q /s
call mvn clean test
call allure generate
timeout 1
xcopy "C:\D\EPAM\Internal Project\test-automation_remote\allure-report\history" "C:\D\EPAM\Internal Project\test-automation_remote\allure-results\history\" /E /Y /F