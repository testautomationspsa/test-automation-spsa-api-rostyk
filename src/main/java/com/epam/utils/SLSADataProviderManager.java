package com.epam.utils;

import com.epam.model.*;
import org.testng.annotations.DataProvider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.epam.utils.Constants.*;
import static com.epam.utils.DataFactory.*;

public class SLSADataProviderManager {

    @DataProvider(name = "correctCoordinates", parallel = true)
    public Object[][] getCorrectCoordinates() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/coordinates.csv", ",");
    }

    @DataProvider(name = "incorrectCoordinates", parallel = true)
    public Object[][] getIncorrectCoordinates() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_INCORRECT_DATA_PATH + "/coordinates.csv", ",");
    }

    @DataProvider(name = "correctLocationId", parallel = true)
    public Object[][] getCorrectLocationId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationId.csv", ",");
    }

    @DataProvider(name = "correctCoachId", parallel = true)
    public Object[][] getCorrectCoachId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/coachId.csv", ",");
    }

    @DataProvider(name = "incorrectId", parallel = true)
    public Object[][] getIncorrectId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/id.csv", ",");
    }

    @DataProvider(name = "correctLocationAndCoachId", parallel = true)
    public Object[][] getCorrectCoachAndLocationId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationAndCoachId.csv", ",");
    }

    @DataProvider(name = "incorrectLocationAndCoachId", parallel = true)
    public Object[][] getIncorrectCoachAndLocationId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_INCORRECT_DATA_PATH + "/locationAndCoachId.csv", ",");
    }

    @DataProvider(name = "coaches")
    public Object[][] getCoaches() throws IOException {
        List<User> users = getNewUsers();
        List<Coach> newCoaches = getNewCoaches();
        List<Coach> coachesToEdit = getCoachesToEdit();
        Object[][] locationId = getCorrectLocationId();
        Object[][] newLocationId = getNewLocationId();
        Object[][] coaches = new Object[users.size()][];
        for (int i = 0; i < users.size(); i++) {
            coaches[i] = new Object[]{users.get(i), locationId[i][0], newCoaches.get(i), coachesToEdit.get(i),
                    newLocationId[i][0]};
        }
        return coaches;
    }

    @DataProvider(name = "correctLocationTypeId", parallel = true)
    public Object[][] getCorrectLocationTypeId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationTypeId.csv", ",");
    }

    @DataProvider(name = "locationIdWithoutSchedule", parallel = true)
    public Object[][] getLocationIdWithoutSchedule() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationIdWithOutSchedule.csv", ",");
    }

    @DataProvider(name = "locations", parallel = true)
    public Object[][] getNewLocationAndToEdit() throws IOException {
        List<Location> newLocation = getNewLocation();
        Object[][] locations= new Object[newLocation.size()][];
        for (int i = 0; i < newLocation.size(); i++) {
            locations[i] = new Object[]{newLocation.get(i), getLocationToEdit().get(i)};
        }
        return locations;
    }

    @DataProvider(name = "coachIdWithComments", parallel = true)
    public Object[][] getCoachIdWithComments() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/coachIdWithComments.csv", ",");
    }

    @DataProvider(name = "comments", parallel = true)
    public Object[][] getNewCommentsAndToEdit() throws IOException {
        List<Comment> newComments = getNewComments();
        Object[][] coachIds = getCorrectCoachId();
        Object[][] comments = new Object[newComments.size()][];
        for (int i = 0; i < comments.length; i++) {
            comments[i] = new Object[]{coachIds[0][0], newComments.get(i), getCommentsToEdit().get(i)};
        }
        return comments;
    }

    @DataProvider(name = "criteria", parallel = true)
    public Object[][] getCriteria() throws IOException {
        List<LocationCriteria> newCriteria = getNewLocationCriteria();
        Object[][] criteria = new Object[newCriteria.size()][];
        for (int i = 0; i < newCriteria.size(); i++) {
            criteria[i] = new Object[]{newCriteria.get(i), getLocationCriteriaToEdit().get(i)};
        }
        return criteria;
    }

    @DataProvider(name = "criteriaToFind", parallel = true)
    public Object[][] getCriteriaToFind() throws IOException {
        List<LocationCriteria> locationCriteria = getLocationCriteria();
        Object[][] criteria = new Object[locationCriteria.size()][];
        for (int i = 0; i < locationCriteria.size(); i++) {
            criteria[i] = new Object[]{locationCriteria.get(i)};
        }
        return criteria;
    }

    @DataProvider(name = "nNewLocationId", parallel = true)
    public Object[][] getNewLocationId() throws IOException {
        return CSVReader.readFromCSV(BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newLocationId.csv", ",");
    }

}
