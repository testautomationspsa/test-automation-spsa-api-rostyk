package com.epam.utils;

public class Constants {
    public static final String BASE_TEST_DATA_PATH = "src/test/resources/test_data";
    public static final String BASE_SPSA_TEST_DATA_PATH = BASE_TEST_DATA_PATH + "/spsa";
    public static final String BASE_SPSA_TEST_CORRECT_DATA_PATH = BASE_SPSA_TEST_DATA_PATH + "/correct";
    public static final String BASE_SPSA_TEST_INCORRECT_DATA_PATH = BASE_SPSA_TEST_DATA_PATH + "/incorrect";
    public static final String BASE_SLSA_TEST_DATA_PATH = BASE_TEST_DATA_PATH + "/slsa";
    public static final String BASE_SLSA_TEST_CORRECT_DATA_PATH = BASE_SLSA_TEST_DATA_PATH + "/correct";
    public static final String BASE_SLSA_TEST_INCORRECT_DATA_PATH = BASE_SLSA_TEST_DATA_PATH + "/incorrect";
    public static final String USERS_ENDPOINT = "/api/v1/users";
    public static final String USER_ENDPOINT = "/api/v1/user";
    public static final String SPORT_TYPE_ENDPOINT = "/api/v1/sportTypes";
    public static final String ADDRESSES_ENDPOINT = "/api/v1/addresses";
    public static final String BLOG_ENDPOINT = "/api/v1/blogs";
    public static final String TAG_ENDPOINT = "/api/v1/tags";
    public static final String LOCATION_ENDPOINT = "/api/v1/locations";
    public static final String COACH_ENDPOINT = "/api/v1/coaches";
    public static final String LOCATION_TYPE_ENDPOINT = "/api/v1/locationTypes";
    public static final String ROLE_ENDPOINT = "/api/v1/roles";
    public static final String ADMIN_ENDPOINT = "/api/v1/admin";
    public static final String FORM_ENDPOINT = "/api/v1/forms";
    public static final String EVENT_ENDPOINT = "/api/v1/events";
    public static final String DEFAULT_PASSWORD = "testTest0";
    public static final double MIN_COORDINATE_VALUE = -180;
    public static final double MAX_COORDINATE_VALUE = 180;
    public static final double MIN_RUNNING_DISTANCE = 0;
    public static final double MAX_RUNNING_DISTANCE = 50;
    public static final double MIN_USER_AGE = 12;
    public static final double MAX_USER_AGE = 100;
    public static final double MIN_COACH_RATE = 0;
    public static final double MAX_COACH_RATE = 5;
}
