package com.epam.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Objects.nonNull;

public class CSVReader {

    public static Object[][] readFromCSV(String filePath, String delimiter) throws IOException {
        List<Object[]> data = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line = "";
            while (nonNull((line = reader.readLine()))) {
                if (!line.startsWith(";")) {
                    data.add(line.split(delimiter));
                }
            }
        }
        return data.toArray(new Object[data.size()][]);
    }
}
