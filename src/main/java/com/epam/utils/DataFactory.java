package com.epam.utils;

import com.epam.model.*;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import static com.epam.utils.Constants.*;

public class DataFactory {
    private static final Gson GSON = new Gson();

    public static List<Criteria> getCriteriaList() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(
                new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/criteriaList.json"), Criteria[].class));
    }

    public static Criteria getCriteria() throws FileNotFoundException {
        return GSON.fromJson(new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/criteria.json"), Criteria.class);
    }

    public static List<User> getNewUsers() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(
                new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/newUsers.json"), User[].class));
    }

    public static List<User> getUsersToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(
                new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/usersToEdit.json"), User[].class));
    }

    public static List<Article> getArticles() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(
                new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/articles.json"), Article[].class));
    }

    public static List<Article> getArticlesToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(
                new FileReader(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/articlesToEdit.json"), Article[].class));
    }

    public static File getImage() {
        return new File(BASE_SPSA_TEST_DATA_PATH + "/testImage.jpg");
    }

    public static File getImageToEdit() {
        return new File(BASE_SPSA_TEST_DATA_PATH + "/imageToEdit.jpg");
    }

    public static String getImageUrlToEdit(){
        return "https://c7.hotpng.com/preview/502/169/189/surgical-mask-dust-mask-surgery-surgeon-mask.jpg";
    }

    public static String getImageUrl(){
        return "https://pngimage.net/wp-content/uploads/2018/06/png-small-2.png";
    }

    public static List<Coach> getCoachesToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/coachesToEdit.json"), Coach[].class));
    }

    public static List<Coach> getNewCoaches() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newCoaches.json"), Coach[].class));
    }

    public static List<LocationSchedule> getLocationScheduleToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationScheduleToEdit.json"), LocationSchedule[].class));
    }

    public static List<LocationSchedule> getNewLocationSchedule() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newLocationSchedule.json"), LocationSchedule[].class));
    }

    public static List<Location> getLocationToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationToEdit.json"), Location[].class));
    }

    public static List<Location> getNewLocation() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newLocation.json"), Location[].class));
    }

    public static List<Comment> getCommentsToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/commentsToEdit.json"), Comment[].class));
    }

    public static List<Comment> getNewComments() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newComments.json"), Comment[].class));
    }

    public static List<LocationCriteria> getLocationCriteriaToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/locationCriteriaToEdit.json"), LocationCriteria[].class));
    }

    public static List<LocationCriteria> getNewLocationCriteria() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/newLocationCriteria.json"), LocationCriteria[].class));
    }

    public static List<LocationCriteria> getLocationCriteria() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SLSA_TEST_CORRECT_DATA_PATH + "/criteriaToFind.json"), LocationCriteria[].class));
    }

    public static List<Form> getFormList() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_CORRECT_DATA_PATH + "/forms.json"), Form[].class));
    }

    public static List<Statistics> getNewStatistics() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_CORRECT_DATA_PATH + "/newStatistics.json"), Statistics[].class));
    }

    public static List<Statistics> getStatisticsToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_CORRECT_DATA_PATH + "/statisticsToEdit.json"), Statistics[].class));
    }

    public static List<Event> getNewEvents() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_CORRECT_DATA_PATH + "/newEvents.json"), Event[].class));
    }

    public static List<Event> getIncorrectEvents() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/newEvents.json"), Event[].class));
    }

    public static List<Event> getEventsToEdit() throws FileNotFoundException {
        return Arrays.asList(GSON.fromJson(new FileReader(
                BASE_SPSA_TEST_CORRECT_DATA_PATH + "/eventsToEdit.json"), Event[].class));
    }
}
