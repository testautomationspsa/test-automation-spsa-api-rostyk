package com.epam.utils;

import com.epam.model.*;
import org.testng.annotations.DataProvider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.epam.utils.Constants.BASE_SPSA_TEST_CORRECT_DATA_PATH;
import static com.epam.utils.Constants.BASE_SPSA_TEST_INCORRECT_DATA_PATH;
import static com.epam.utils.DataFactory.*;

public class SPSADataProviderManager {

    @DataProvider(name = "correctUsersId", parallel = true)
    public Object[][] getCorrectUsersId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/userId.csv", ",");
    }

    @DataProvider(name = "correctCoordinates", parallel = true)
    public Object[][] getCorrectCoordinates() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/coordinates.csv", ",");
    }

    @DataProvider(name = "incorrectCoordinates", parallel = true)
    public Object[][] getIncorrectCoordinates() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/coordinates.csv", ",");
    }

    @DataProvider(name = "criteriaAndCorrectUserId")
    public Object[][] getCriteriaAndCorrectUserId() throws IOException {
        return mergeIdsAndCriteria(getCorrectUserIdForCriteria());
    }

    @DataProvider(name = "criteriaAndIncorrectUserId")
    public Object[][] getCriteriaAndIncorrectUserId() throws IOException {
        return mergeIdsAndCriteria(getIncorrectId());
    }

    private Object[][] mergeIdsAndCriteria(Object[][] ids) throws FileNotFoundException {
        Object[][] criteriaAndId = new Object[ids.length][];
        for (int i = 0; i < ids.length; i++) {
            criteriaAndId[i] = new Object[]{ids[i][0], getCriteria()};
        }
        return criteriaAndId;
    }

    @DataProvider(name = "criteriaAndIncorrectUserIdAndPageAndSize")
    public Object[][] getCriteriaAndIncorrectUserIdAndPageAndSize() throws IOException {
        return mergeIdsAndCriteriaAndPageAndSize(getCorrectUserIdForCriteria(), getCorrectPagesAndSizes());
    }

    private Object[][] mergeIdsAndCriteriaAndPageAndSize(Object[][] ids, Object[][] pageAndSize) throws FileNotFoundException {
        Object[][] criteriaAndIdAndPageAndSize = new Object[ids.length][];
        List<Criteria> criteria = getCriteriaList();
        for (int i = 0; i < ids.length; i++) {
            criteriaAndIdAndPageAndSize[i] = new Object[]{ids[i][0], criteria.get(i), pageAndSize[i][0], pageAndSize[i][1]};
        }
        return criteriaAndIdAndPageAndSize;
    }

    @DataProvider(name = "correctUserIdForCriteria")
    public Object[][] getCorrectUserIdForCriteria() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/userIdForCriteria.csv", ",");
    }

    @DataProvider(name = "correctSportTypeId", parallel = true)
    public Object[][] getCorrectSportTypeId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/sportTypeId.csv", ",");
    }

    @DataProvider(name = "users", parallel = true)
    public Object[][] getUsers() throws IOException {
        List<User> newUsers = getNewUsers();
        List<User> usersToEdit = DataFactory.getUsersToEdit();
        List<Statistics> newStatistics = getNewStatistics();
        List<Statistics> statisticsToEdit = getStatisticsToEdit();
        List<Form> forms = getFormList();
        Object[][] pages = getCorrectPagesAndSizes();
        Object[][] users = new Object[newUsers.size()][];
        for (int i = 0; i < users.length; i++) {
            users[i] = new Object[]{newUsers.get(i), usersToEdit.get(i), newStatistics.get(i), statisticsToEdit.get(i),
                    forms.get(i), pages[i][0], pages[i][1]};
        }
        return users;
    }

    @DataProvider(name = "correctTagId", parallel = true)
    public Object[][] getCorrectTagId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/tagId.csv", ",");
    }

    @DataProvider(name = "tags", parallel = true)
    public Object[][] getTags() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/tags.csv", ",");
    }

    @DataProvider(parallel = true)
    public Object[][] getCorrectTagsPagesAndSizes() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/tagsPagesAndSizes.csv", ",");
    }

    @DataProvider(parallel = true)
    public Object[][] getIncorrectTagsPagesAndSizes() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/tagsPagesAndSizes.csv", ",");
    }

    @DataProvider(parallel = true)
    public Object[][] getCorrectPagesAndSizes() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/pageAndSize.csv", ",");
    }

    @DataProvider(parallel = true)
    public Object[][] getIncorrectPagesAndSizes() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/pageAndSize.csv", ",");
    }

    @DataProvider(parallel = true)
    public Object[][] getCorrectArticleId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/articleId.csv", ",");
    }

    @DataProvider(name = "articles", parallel = true)
    public Object[][] getArticles() throws IOException {
        List<Article> newArticles = DataFactory.getArticles();
        List<Article> articlesToEdit = DataFactory.getArticlesToEdit();
        Object[][] articles = new Object[newArticles.size()][];
        for (int i = 0; i < articles.length; i++) {
            articles[i] = new Object[]{newArticles.get(i), articlesToEdit.get(i)};
        }
        return articles;
    }

    @DataProvider(name = "articlesToEdit", parallel = true)
    public Object[][] getArticlesToEdit() throws IOException {
        return transformListToMatrix(DataFactory.getArticlesToEdit());
    }

    private <E>Object[][] transformListToMatrix(List<E> list){
        Object[][] obj = new Object[list.size()][];
        for (int i = 0; i < obj.length; i++) {
            obj[i] = new Object[]{list.get(i)};
        }
        return obj;
    }

    @DataProvider(name = "correctEmailAndPassword", parallel = true)
    public Object[][] getCorrectEmailAndPassword() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/emailAndPassword.csv", ",");
    }

    @DataProvider(name = "incorrectEmailAndPassword", parallel = true)
    public Object[][] getIncorrectEmailAndPassword() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/emailAndPassword.csv", ",");
    }

    @DataProvider(name = "correctRoleId", parallel = true)
    public Object[][] getCorrectRoleId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/roleId.csv", ",");
    }

    @DataProvider(name = "incorrectId", parallel = true)
    public Object[][] getIncorrectId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_INCORRECT_DATA_PATH + "/id.csv", ",");
    }

    @DataProvider(name = "correctAdminId", parallel = true)
    public Object[][] getCorrectAdminId() throws IOException {
        return CSVReader.readFromCSV(BASE_SPSA_TEST_CORRECT_DATA_PATH + "/adminId.csv", ",");
    }

    @DataProvider(name = "forms", parallel = true)
    public Object[][] getForms() throws IOException {
        List<Form> forms = getFormList();
        Object[][] users = getCorrectEmailAndPassword();
        Object[][] formsObj = new Object[forms.size()][];
        for (int i = 0; i < formsObj.length; i++) {
            formsObj[i] = new Object[]{forms.get(i), users[i][0], users[i][1], "1", "20"};
        }
        return formsObj;
    }

    @DataProvider(name = "correctComments")
    public Object[][] getCorrectComments() throws IOException {
        return getComments(getCorrectArticleId(), getCorrectUsersId());
    }

    @DataProvider(name = "incorrectComments", parallel = true)
    public Object[][] getIncorrectComments() throws IOException {
        return getComments(getIncorrectId(), getIncorrectId());
    }

    private Object[][] getComments(Object[][] blogId , Object[][] userId){
        Object[][] comments = new Object[userId.length][];
        for (int i = 0; i < userId.length; i++) {
            comments[i] = new Object[]{blogId[i][0], userId[i][0], "This is a comment!"+i};
        }
        return comments;
    }

    @DataProvider(name = "events", parallel = true)
    public Object[][] getEvents() throws IOException {
        List<Event> newEvents = getNewEvents();
        List<Event> eventsToEdit = getEventsToEdit();
        Object[][] events = new Object[newEvents.size()][];
        for (int i = 0; i < events.length; i++) {
            events[i] = new Object[]{newEvents.get(i), eventsToEdit.get(i)};
        }
        return events;
    }

    @DataProvider(name = "incorrectEvents", parallel = true)
    public Object[][] getIncorrectEvents() throws IOException {
        List<Event> incorrectEvents = DataFactory.getIncorrectEvents();
        Object[][] events = new Object[incorrectEvents.size()][];
        for (int i = 0; i < events.length; i++) {
            events[i] = new Object[]{incorrectEvents.get(i)};
        }
        return events;
    }
}
