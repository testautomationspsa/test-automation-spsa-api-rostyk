package com.epam.utils;

import com.epam.model.*;
import com.epam.model.Role;
import com.epam.model.enums.*;
import io.qameta.allure.Step;
import org.apache.commons.lang3.EnumUtils;

import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.epam.utils.Constants.*;
import static com.epam.utils.Constants.MAX_RUNNING_DISTANCE;
import static java.util.Objects.nonNull;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ValidationManager {
    @Step("Verifying Address = {0} step...")
    public static void verifyAddress(Address address) {
        if (address == null) return;
        verifyCoordinate(address.getLatitude());
        verifyCoordinate(address.getLongitude());
    }

    @Step("Verifying coordinate = {0} step...")
    private static void verifyCoordinate(double coordinate) {
        assertTrue(coordinate <= MAX_COORDINATE_VALUE && coordinate >= MIN_COORDINATE_VALUE,
                coordinate + " is not in range: [ " + MIN_COORDINATE_VALUE + " ; " + MAX_COORDINATE_VALUE + " ]");
    }

    @Step("Verifying Criteria = {0} step...")
    public static void verifyCriteria(Criteria criteria) {
        assertTrue(EnumUtils.isValidEnum(ActivityTime.class, criteria.getActivityTime().toUpperCase()),
                criteria.getActivityTime() + " is not a valid ActivityTime");
        assertTrue(EnumUtils.isValidEnum(Gender.class, criteria.getGender().toUpperCase()),
                criteria.getGender() + " is not a valid Gender");
        assertTrue(EnumUtils.isValidEnum(Maturity.class, criteria.getMaturity().toUpperCase()),
                criteria.getMaturity() + " is not a valid Maturity");
        verifySportType(criteria.getSportType());
        assertTrue(criteria.getRunningDistance() >= MIN_RUNNING_DISTANCE &&
                        criteria.getRunningDistance() <= MAX_RUNNING_DISTANCE,
                criteria.getRunningDistance() + " is not in range: ["
                        + MIN_RUNNING_DISTANCE + ";" + MAX_RUNNING_DISTANCE + "]");
    }

    @Step("Verifying Partner = {0} step...")
    public static void verifyUser(User user) {
        if (nonNull(user.getGender())) {
            assertTrue(EnumUtils.isValidEnum(Gender.class, user.getGender().toUpperCase()),
                    user.getGender() + " is not a valid Gender");
        }
        verifyAddress(user.getAddress());
        if (nonNull(user.getDateOfBirth())) {
            int age = getAge(user.getDateOfBirth());
            assertTrue(age >= MIN_USER_AGE && age <= MAX_USER_AGE, age + " is not a valid age(valid is:[ "
                    + MIN_USER_AGE + " ; " + MAX_USER_AGE + "]");
        }
        verifyEmail(user.getEmail());
        verifyPhoneNumber(user.getPhoneNumber());
        verifyName(user.getName(), "Name");
        verifyName(user.getSurname(), "Surname");
    }

    private static void verifyPhoneNumber(String number) {
        if (number == null) return;
        assertTrue(number.matches("^((\\+38)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$"),
                number + " is not a valid Phone Number ");
    }

    private static int getAge(String date) {
        LocalDate dateOfBirth = LocalDate.parse(date);
        LocalDate now = LocalDate.now();
        return Period.between(dateOfBirth, now).getYears();
    }

    @Step("Verifying SportType = {0} step...")
    public static void verifySportType(SportType sportType) {
        assertTrue(EnumUtils.isValidEnum(SportTypeEnum.class, sportType.getName().toUpperCase()),
                sportType.getName() + " is not a valid SportType");
    }

    @Step("Verifying CriteriaUser = {0} step...")
    public static void verifyCriteriaUser(CriteriaUser criteriaUser) {
        verifyName(criteriaUser.getName(), "Name");
        verifyName(criteriaUser.getCity(), "City");
        verifyEmail(criteriaUser.getEmail());
        assertTrue(EnumUtils.isValidEnum(Gender.class, criteriaUser.getGender().toUpperCase()),
                criteriaUser.getGender() + " is not a valid Gender");
        ValidationManager.verifyCriteria(criteriaUser.getCriteriaDto());
    }

    @Step("Verifying {1} = {0} step...")
    private static void verifyName(String value, String name) {
        if (value == null) return;
        assertTrue(value.matches("^[a-zA-z']{2,20}$"), value + " is not a valid " + name);
    }

    @Step("Verifying Email = {0} step...")
    private static void verifyEmail(String value) {
        assertTrue(value.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$"),
                value + " is not a valid Email");
    }

    @Step("Verifying Article = {0} step...")
    public static void verifyArticle(Article article) {
        verifyName(article.getAuthorName(), "AuthorName");
        verifyName(article.getAuthorSurname(), "AuthorSurname");
        assertNotNull(article.getContent());
        if (nonNull(article.getPictureUrl())) {
            assertTrue(article.getPictureUrl()
                            .matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
                    , article.getPictureUrl() + " is not a valid Picture URL!");
        }
        assertTrue(article.getTopic().matches(".{5,100}"),
                article.getTopic() + "is not a valid Topic!");
        article.getTags().forEach(ValidationManager::verifyTag);
    }

    @Step("Verifying Tag = {0} step...")
    public static void verifyTag(Tag tag) {
        verifyName(tag.getName(), "Tag");
    }

    @Step("Verifying Coach = {0} step...")
    public static void verifyCoach(Coach coach) {
        verifyRate(coach.getRating());
        coach.getSportTypes().forEach(ValidationManager::verifySportType);
        coach.getLinks().forEach(ValidationManager::verifyLink);
    }

    @Step("Verifying rate = {0} step...")
    private static void verifyRate(int rate) {
        assertTrue(rate >= MIN_COACH_RATE && rate <= MAX_COACH_RATE,
                rate + " rate is not in a range: [" + MIN_COACH_RATE + ";" + MAX_COACH_RATE + "]");
    }

    @Step("Verifying link = {0} step...")
    public static void verifyLink(Link link) {
        assertTrue(EnumUtils.isValidEnum(LinkType.class, link.getType().toUpperCase()),
                link.getType() + " is not a valid Link type");
    }

    @Step("Verifying LocationType = {0} step...")
    public static void verifyLocationType(LocationType locationType) {
        assertTrue(locationType.getName().length() >= 3 && locationType.getName().length() <= 32,
                "LocationType name is too short or too long");
        assertTrue(EnumUtils.isValidEnum(Placing.class, locationType.getPlacing().toUpperCase()),
                locationType.getPlacing() + " is not a valid Placing");
    }

    @Step("Verifying LocationSchedule = {0} step...")
    public static void verifyLocationSchedule(LocationSchedule schedule) {
        verifyTime(schedule.getEndTime());
        verifyTime(schedule.getStartTime());
        assertTrue(EnumUtils.isValidEnum(Day.class, schedule.getDay().toUpperCase()),
                schedule.getDay() + " is not a valid Day");
    }

    @Step("Verifying time = {0} step...")
    private static void verifyTime(String time) {
        assertTrue(time.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]$"),
                time + " time is not a valid Time");
    }

    @Step("Verifying Location = {0} step...")
    public static void verifyLocation(Location location) {
        verifyAddress(location.getAddress());
        verifyLocationName(location.getName());
        location.getSportTypes().forEach(ValidationManager::verifySportType);
        verifyLocationType(location.getLocationType());
        location.getLocationSchedule().forEach(ValidationManager::verifyLocationSchedule);
        location.getCoaches().forEach(ValidationManager::verifyCoach);
    }

    @Step("Verifying location name = {0} ")
    private static void verifyLocationName(String name) {
        Pattern pattern = Pattern.compile("^[,.«»а-яА-Яієї:-a-zA-z \"'ʹ]{5,128}$",
                Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(name);
        assertTrue(matcher.find(), name + " is not a valid Location name");
    }

    @Step("Verifying Comment = {0} step...")
    public static void verifyComment(Comment comment) {
        verifyRate(comment.getRating());
        assertTrue(comment.getComment().matches(".{5,150}"),
                comment.getComment() + " is not a valid Comment");
    }

    @Step("Verifying LocationCriteria = {0} step...")
    public static void verifyLocationCriteria(LocationCriteria criteria) {
        verifyCoordinate(criteria.getLatitude());
        verifyCoordinate(criteria.getLongitude());
        assertTrue(EnumUtils.isValidEnum(Placing.class, criteria.getPlacing().toUpperCase()),
                criteria.getPlacing() + " is not a valid Placing");
        criteria.getSportTypes().forEach(ValidationManager::verifySportType);
    }

    @Step("Verifying Role = {0} step...")
    public static void verifyRole(Role role) {
        assertTrue(EnumUtils.isValidEnum(RoleEnum.class, role.getName().toUpperCase()),
                role.getName() + " is not a valid Role");
    }

    @Step("Verifying Form = {0} step...")
    public static void verifyForm(Form form) {
        verifyRole(form.getRole());
        verifyAddress(form.getAddress());
        verifyLocationName(form.getLocationName());
    }

    @Step("Verifying Statistics = {0} step...")
    public static void verifyStatistics(Statistics statistics) {
        verifySportType(statistics.getSportType());
        assertTrue(statistics.getResultH().matches("^PT([0-9]+H){0,1}([0-9]+M){0,1}([0-9]+S){0,1}$"),
                statistics.getResultH()+" is Wrong result!!");
        assertTrue(statistics.getResultKm() >= MIN_RUNNING_DISTANCE && statistics.getResultKm()
                <= 4 * MAX_RUNNING_DISTANCE, "Result cannot be more than 200KM");
    }

}
