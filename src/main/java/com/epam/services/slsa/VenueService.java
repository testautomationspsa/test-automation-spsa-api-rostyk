package com.epam.services.slsa;

import com.epam.model.LocationCriteria;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.LOCATION_ENDPOINT;

public class VenueService extends Service {

    @Step("Getting matched list of venues with criteria = {0} step...")
    public ValidatableResponse viewMatchedListOfVenues(LocationCriteria criteria) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(criteria)
                .post(LOCATION_ENDPOINT + "/criteria/venues"));
    }
}
