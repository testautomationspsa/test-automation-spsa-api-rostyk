package com.epam.services.slsa;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.ResourceBundle;

import static io.restassured.RestAssured.given;

public class Service {
    protected RequestSpecification request;

    public Service() {
        request = given()
                .log()
                .all()
                .baseUri(ResourceBundle.getBundle("config").getString("baseSLSAUrl"))
                .filter(new AllureRestAssured());
    }

    protected ValidatableResponse validateResponse(Response response) {
        return response
                .then()
                .log()
                .all();
    }
}
