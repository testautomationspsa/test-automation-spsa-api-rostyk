package com.epam.services.slsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.LOCATION_TYPE_ENDPOINT;

public class LocationTypeService extends Service {

    @Step("Getting all location types step...")
    public ValidatableResponse getAllLocationTypes() {
        return validateResponse(request.get(LOCATION_TYPE_ENDPOINT));
    }

    @Step("Getting location type by id = {0} step...")
    public ValidatableResponse getLocationTypeById(String id) {
        return validateResponse(request.get(LOCATION_TYPE_ENDPOINT + "/" + id));
    }
}
