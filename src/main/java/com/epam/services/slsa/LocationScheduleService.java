package com.epam.services.slsa;

import com.epam.model.LocationSchedule;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.util.List;

import static com.epam.utils.Constants.LOCATION_ENDPOINT;

public class LocationScheduleService extends Service {

    @Step("Getting location schedule by id = {0} step...")
    public ValidatableResponse getLocationScheduleById(String locationId) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/" + locationId + "/locationSchedule"));
    }

    @Step("Adding location schedule = {1} with location_Id = {0} step...")
    public ValidatableResponse addLocationSchedule(String locationId, List<LocationSchedule> schedule) {
        return validateResponse(request
                .body(schedule)
                .contentType(ContentType.JSON)
                .post(LOCATION_ENDPOINT + "/" + locationId + "/locationSchedule"));
    }

    @Step("Updating location schedule with location_Id = {0} to {1} step...")
    public ValidatableResponse editLocationSchedule(String locationId, List<LocationSchedule> schedule) {
        return validateResponse(request
                .body(schedule)
                .contentType(ContentType.JSON)
                .put(LOCATION_ENDPOINT + "/" + locationId + "/locationSchedule"));
    }

    @Step("Deleting location schedule with location_Id = {0} step...")
    public ValidatableResponse deleteLocationSchedule(String locationId) {
        return validateResponse(request
                .delete(LOCATION_ENDPOINT + "/locationSchedule/" + locationId));
    }
}
