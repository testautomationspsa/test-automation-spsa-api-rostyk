package com.epam.services.slsa;

import com.epam.model.Location;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.io.File;

import static com.epam.utils.Constants.LOCATION_ENDPOINT;

public class LocationService extends Service {

    @Step("Getting all locations step...")
    public ValidatableResponse getAllLocations() {
        return validateResponse(request.get(LOCATION_ENDPOINT));
    }

    @Step("Getting location by id = {0} step...")
    public ValidatableResponse getLocationById(String id) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/" + id));
    }

    @Step("Getting list of admin`s location by adminId = {0} step...")
    public ValidatableResponse getAdminLocationsById(String adminId) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/admin/" + adminId));
    }

    @Step("Adding new location = {1} step...")
    public ValidatableResponse addLocation(Location location) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(location)
                .post(LOCATION_ENDPOINT));
    }

    @Step("Adding location`s image with id = {1} step...")
    public ValidatableResponse addLocationImage(File image, String id) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .post(LOCATION_ENDPOINT + "/" + id + "/images"));
    }

    @Step("Adding location`s image-url = {0} with id = {1} step...")
    public ValidatableResponse addLocationImageUrl(String url, String id) {
        return validateResponse(request
                .post(LOCATION_ENDPOINT + "/" + id + "/url-images?url=" + url));
    }

    @Step("Editing location with id = {0} to {1} step...")
    public ValidatableResponse editLocation(String locationId, Location location) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(location)
                .put(LOCATION_ENDPOINT + "/" + locationId));
    }

    @Step("Updating location`s(id = {1}) image with id = {2} step...")
    public ValidatableResponse editLocationImage(File image, String locationId, String imageId) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .put(LOCATION_ENDPOINT + "/" + locationId + "/images/" + imageId));
    }

    @Step("Updating location`s(id = {1}) image-url = {0} with id = {2} step...")
    public ValidatableResponse editLocationImageUrl(String url, String locationId, String imageId) {
        return validateResponse(request
                .put(LOCATION_ENDPOINT + "/" + locationId + "/url-images/" + imageId + "?url=" + url));
    }

    @Step("Deleting location with id = {0} step...")
    public ValidatableResponse deleteLocation(String locationId) {
        return validateResponse(request.delete(LOCATION_ENDPOINT + "/" + locationId));
    }

    @Step("Deleting location`s image with locationId = {0} and imageId = {1} step...")
    public ValidatableResponse deleteLocationImage(String locationId, String imageId) {
        return validateResponse(request.delete(LOCATION_ENDPOINT + "/" + locationId+"/images/" + imageId));
    }

}
