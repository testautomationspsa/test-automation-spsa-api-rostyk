package com.epam.services.slsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.*;

public class AddressService extends Service {

    @Step("Getting address by coordinates lat = {0} and long = {1} step...")
    public ValidatableResponse getAddressByCoordinates(String latitude, String longitude) {
        return validateResponse(request.get(ADDRESSES_ENDPOINT
                + "/coordinates?lat=" + latitude + "&long=" + longitude));
    }

    @Step("Getting address by users id = {0} step...")
    public ValidatableResponse getAddressByLocationId(String id) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/" + id + "/address"));
    }
}
