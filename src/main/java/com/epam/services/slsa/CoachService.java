package com.epam.services.slsa;

import com.epam.model.Coach;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.io.File;

import static com.epam.utils.Constants.COACH_ENDPOINT;
import static com.epam.utils.Constants.LOCATION_ENDPOINT;

public class CoachService extends Service {

    @Step("Getting all coaches by location Id = {0} step...")
    public ValidatableResponse getAllCoachesByLocationId(String id) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/" + id + "/coaches"));
    }

    @Step("Getting coach by id = {1} and location Id = {0} step...")
    public ValidatableResponse getCoachById(String locationId, String coachId) {
        return validateResponse(request.get(LOCATION_ENDPOINT + "/" + locationId + "/coaches/" + coachId));
    }

    @Step("Getting coach by userId = {1} step...")
    public ValidatableResponse getCoachByUserId(String userId) {
        return validateResponse(request.get(COACH_ENDPOINT + "/user/" + userId));
    }

    @Step("Adding new coach = {1} by location Id = {0} step...")
    public ValidatableResponse addCoach(String locationId, Coach coach) {
        return validateResponse(request
                .body(coach)
                .contentType("application/json")
                .post(LOCATION_ENDPOINT + "/" + locationId + "/set/coach"));
    }

    @Step("Setting new location( id = {1} ) to the coach with id = {0} step...")
    public ValidatableResponse setNewCoachLocation(String coachId, String locationId) {
        return validateResponse(request
                .post(COACH_ENDPOINT + "/" + coachId + "/new-location?location_id=" + locationId));
    }

    @Step("Editing coach with id = {1} by location Id = {0} to {2} step...")
    public ValidatableResponse editCoach(String locationId, String coachId, Coach coach) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(coach)
                .put(LOCATION_ENDPOINT + "/" + locationId + "/coaches/" + coachId));
    }

    @Step("Deleting coach with id = {1} by location id = {0} step...")
    public ValidatableResponse deleteCoach(String coachId) {
        return validateResponse(request.delete(COACH_ENDPOINT + "/" + coachId));
    }

    @Step("Deleting coach`s link by linkId = {0} step...")
    public ValidatableResponse deleteLinkById(String linkId) {
        return validateResponse(request.delete(COACH_ENDPOINT + "/links/" + linkId));
    }

}
