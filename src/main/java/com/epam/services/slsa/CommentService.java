package com.epam.services.slsa;

import com.epam.model.Comment;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.COACH_ENDPOINT;

public class CommentService extends Service {

    @Step("Getting all comments by coachId = {0} step...")
    public ValidatableResponse getAllCommentsByCoachId(String id, String page, String size) {
        return validateResponse(request.get(COACH_ENDPOINT + "/" + id + "/comments?page=" + page + "&size=" + size));
    }

    @Step("Adding new comment = {1} with coachId = {0} step...")
    public ValidatableResponse addNewComment(String coachId, Comment comment) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(comment)
                .post(COACH_ENDPOINT + "/" + coachId + "/comment"));
    }

    @Step("Updating comment with id = {1} and coachId = {0} to {1} step...")
    public ValidatableResponse editComment(String coachId, String commentId, Comment comment) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(comment)
                .put(COACH_ENDPOINT + "/" + coachId + "/comments/" + commentId));
    }

    @Step("Deleting comment with userId = {0} step...")
    public ValidatableResponse deleteCommentByUserId(String id) {
        return validateResponse(request.delete(COACH_ENDPOINT + "/comments/user/" + id));
    }

    @Step("Deleting comment with commentId = {1} and coachId = {0} step...")
    public ValidatableResponse deleteCommentByCommentId(String coachId, String commentId) {
        return validateResponse(request.delete(COACH_ENDPOINT + "/" + coachId + "/comments/" + commentId));
    }
}
