package com.epam.services.spsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.ADMIN_ENDPOINT;

public class AdminService extends Service {

    @Step("Getting locations for venue admin with id = {0} step...")
    public ValidatableResponse getAllLocationsByAdminId(String id) {
        return validateResponse(request.get(ADMIN_ENDPOINT + "/" + id + "/locations"));
    }
}
