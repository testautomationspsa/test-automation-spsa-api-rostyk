package com.epam.services.spsa;

import com.epam.model.Form;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.FORM_ENDPOINT;

public class FormService extends Service {

    @Step("Saving new form = {0} step...")
    public ValidatableResponse saveForm(Form form, String token) {
        return validateResponse(request
                .cookie("AJWT", token)
                .contentType(ContentType.JSON)
                .body(form)
                .post(FORM_ENDPOINT));
    }

    @Step("Getting form by id = {0} step...")
    public ValidatableResponse getFormById(String id) {
        return validateResponse(request.get(FORM_ENDPOINT + "/" + id));
    }

    @Step("Approving form with id = {0} step...")
    public ValidatableResponse approveForm(String id) {
        return validateResponse(request.get(FORM_ENDPOINT + "/approve/" + id));
    }

    @Step("Getting forms by pageNumber = {0} and pageSize = {1} step...")
    public ValidatableResponse getForms(String pageNumber, String pageSize) {
        return validateResponse(request.queryParam("pageNumber", pageNumber)
                .queryParam("pageSize",pageSize)
                .get(FORM_ENDPOINT));
    }

    @Step("Deleting form by id = {0} step...")
    public ValidatableResponse deleteForm(String id,String token) {
        return validateResponse(request.cookie("AJWT", token)
                .delete(FORM_ENDPOINT + "/" + id));
    }

}
