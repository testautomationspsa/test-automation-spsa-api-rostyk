package com.epam.services.spsa;

import com.epam.model.Tag;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.TAG_ENDPOINT;

public class TagService extends Service {

    @Step("Getting all tags step...")
    public ValidatableResponse getAllTags() {
        return validateResponse(request.get(TAG_ENDPOINT + "/"));
    }

    @Step("Getting tag by id = {0} step...")
    public ValidatableResponse getTagById(String id) {
        return validateResponse(request.get(TAG_ENDPOINT + "/" + id));
    }

    @Step("Getting articles by tag = {2}, pageNumber = {0} and pageSIze = {1} step...")
    public ValidatableResponse getArticlesByTag(String pageNumber, String pageSize, String tag) {
        return validateResponse(request.get(TAG_ENDPOINT
                + "/articles?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&tag=" + tag));
    }

    @Step("Adding Tag = {0} step...")
    public ValidatableResponse addTag(Tag tag) {
        return validateResponse(request
                .body(tag)
                .contentType("application/json")
                .post(TAG_ENDPOINT + "/"));
    }

    @Step("Deleting tag with id = {0} step...")
    public ValidatableResponse deleteTag(String id) {
        return validateResponse(request.delete(TAG_ENDPOINT + "/" + id));
    }

}
