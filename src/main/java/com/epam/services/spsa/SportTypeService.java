package com.epam.services.spsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.SPORT_TYPE_ENDPOINT;

public class SportTypeService extends Service {

    @Step("Getting all sport types step ...")
    public ValidatableResponse getAllSportTypes() {
        return validateResponse(request.get(SPORT_TYPE_ENDPOINT+"/"));
    }

    @Step("Getting sport type by id = {0} step ...")
    public ValidatableResponse getSportTypeById(String id) {
        return validateResponse(request.get(SPORT_TYPE_ENDPOINT + "/" + id));
    }
}
