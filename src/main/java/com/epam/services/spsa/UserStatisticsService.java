package com.epam.services.spsa;

import com.epam.model.Statistics;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.epam.utils.Constants.USER_ENDPOINT;

public class UserStatisticsService extends Service {

    @Step("Getting all user`s statistics with id = {0} step...")
    public ValidatableResponse getAllUserStatistics(String userId) {
        return validateResponse(request.get(USER_ENDPOINT + "/" + userId + "/stats"));
    }

    @Step("Getting user`s last statistics with id = {0} step...")
    public ValidatableResponse getUserLastStatistics(String userId) {
        return validateResponse(request.get(USER_ENDPOINT + "/" + userId + "/stats/last"));
    }

    @Step("Adding user`s statistics = {1} with userId = {0} step...")
    public ValidatableResponse saveStatistics(String userId, Statistics statistics) {
        return validateResponse(request
                .body(statistics)
                .contentType(ContentType.JSON)
                .post(USER_ENDPOINT + "/" + userId + "/stats"));
    }

    @Step("Adding user`s statistics = {1} with userId = {0} step...")
    public ValidatableResponse getUserStatisticsByParam(String userId, Statistics statistics) {
        return validateResponse(request
                .body(statistics)
                .contentType(ContentType.JSON)
                .post(USER_ENDPOINT + "/" + userId + "/common-stats"));
    }

    @Step("Updating user`s statistics = {1} with userId = {0} step...")
    public ValidatableResponse editUserLastStatistics(String userId, Statistics statistics) {
        return validateResponse(request
                .body(statistics)
                .contentType(ContentType.JSON)
                .put(USER_ENDPOINT + "/" + userId + "/stats"));
    }
}
