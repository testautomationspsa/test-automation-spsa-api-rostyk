package com.epam.services.spsa;

import com.epam.model.Authentication;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class AuthenticationService extends Service {

    @Step("Logging in with email = {0} and password = {1} step...")
    public ValidatableResponse logIn(String email, String password) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(new Authentication(email, password))
                .post("/authenticate"));
    }

    @Step("Logging out step...")
    public ValidatableResponse logOut() {
        return validateResponse(request
                .post("/logout"));
    }
}
