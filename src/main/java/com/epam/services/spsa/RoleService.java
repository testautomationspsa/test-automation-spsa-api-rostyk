package com.epam.services.spsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.ROLE_ENDPOINT;

public class RoleService extends Service {

    @Step("Getting all roles step...")
    public ValidatableResponse getAllRoles() {
        return validateResponse(request.get(ROLE_ENDPOINT));
    }

    @Step("Getting a role by id = {0} step...")
    public ValidatableResponse getRoleById(String id) {
        return validateResponse(request.get(ROLE_ENDPOINT + "/" + id));
    }
}
