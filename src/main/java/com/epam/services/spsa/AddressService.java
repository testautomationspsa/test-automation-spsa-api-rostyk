package com.epam.services.spsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.ADDRESSES_ENDPOINT;
import static com.epam.utils.Constants.USERS_ENDPOINT;

public class AddressService extends Service {

    @Step("Getting all addresses step...")
    public ValidatableResponse getAllAddresses() {
        return validateResponse(request.get(ADDRESSES_ENDPOINT));
    }

    @Step("Getting address by coordinates lat = {0} and long = {1} step...")
    public ValidatableResponse getAddressByCoordinates(String latitude, String longitude) {
        return validateResponse(request.get(ADDRESSES_ENDPOINT
                + "/coordinates?lat=" + latitude + "&long=" + longitude));
    }

    @Step("Getting address by users id = {0} step...")
    public ValidatableResponse getAddressByUsersId(String id) {
        return validateResponse(request.get(USERS_ENDPOINT + "/" + id + "/address"));
    }
}
