package com.epam.services.spsa;

import com.epam.model.Article;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import java.io.File;

import static com.epam.utils.Constants.BLOG_ENDPOINT;

public class BlogService extends Service {

    @Step("Getting articles by page = {0} and size = {1} step...")
    public ValidatableResponse getArticles(String page, String size) {
        return validateResponse(request.get(BLOG_ENDPOINT + "?page=" + page + "&size=" + size));
    }

    @Step("Getting article by id = {0} step...")
    public ValidatableResponse getArticleById(String id) {
        return validateResponse(request.get(BLOG_ENDPOINT + "/articles/" + id));
    }

    @Step("Adding article = {0} step...")
    public ValidatableResponse addArticle(Article article) {
        return validateResponse(request
                .body(article)
                .contentType("application/json")
                .post(BLOG_ENDPOINT + "/create"));
    }

    @Step("Adding article`s image with id = {1} step...")
    public ValidatableResponse addArticleImage(File image, String id) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .post(BLOG_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Adding article`s image-url = {0} with id = {1} step...")
    public ValidatableResponse addArticleImageUrl(String url, String id) {
        return validateResponse(request
                .post(BLOG_ENDPOINT + "/" + id + "/image-url?url=" + url));
    }

    @Step("Deleting article with id = {0} step...")
    public ValidatableResponse deleteArticle(String id) {
        return validateResponse(request.delete(BLOG_ENDPOINT + "/delete/" + id));
    }

    @Step("Deleting article's image with id = {0} step...")
    public ValidatableResponse deleteArticlesImage(String id) {
        return validateResponse(request.delete(BLOG_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Updating article with id = {1} to article = {0} step...")
    public ValidatableResponse editArticle(Article article, String id) {
        return validateResponse(request
                .body(article)
                .contentType("application/json")
                .put(BLOG_ENDPOINT + "/update/" + id));
    }

    @Step("Updating article`s image with id = {1} step...")
    public ValidatableResponse editArticleImage(File image, String id) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .put(BLOG_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Updating article`s image-url = {0} with id = {1} step...")
    public ValidatableResponse editArticleImageUrl(String url, String id) {
        return validateResponse(request
                .put(BLOG_ENDPOINT + "/" + id + "/image-url?url=" + url));
    }
}
