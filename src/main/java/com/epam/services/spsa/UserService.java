package com.epam.services.spsa;

import com.epam.model.Role;
import com.epam.model.User;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.io.File;

import static com.epam.utils.Constants.USERS_ENDPOINT;

public class UserService extends Service {

    @Step("Getting current sign in user step...")
    public ValidatableResponse getCurrentSignInUser(String token) {
        return validateResponse(request
                .cookie("AJWT", token)
                .get(USERS_ENDPOINT + "/me"));
    }

    @Step("Going online with token = {0} step...")
    public ValidatableResponse goOffline(String token) {
        return validateResponse(request
                .cookie("AJWT", token)
                .get(USERS_ENDPOINT + "/me/status/offline"));
    }

    @Step("Going offline with token = {0}  step...")
    public ValidatableResponse goOnline(String token) {
        return validateResponse(request
                .cookie("AJWT", token)
                .get(USERS_ENDPOINT + "/me/status/online"));
    }

    @Step("Getting all users step ...")
    public ValidatableResponse getAllUsers() {
        return validateResponse(request.get(USERS_ENDPOINT + "/"));
    }

    @Step("Getting user by id = {0} step ...")
    public ValidatableResponse getUserById(String id) {
        return validateResponse(request.get(USERS_ENDPOINT + "/" + id));
    }

    @Step("Adding new user step ...")
    public ValidatableResponse addUser(User user) {
        return validateResponse(request
                .body(user)
                .contentType(ContentType.JSON)
                .post(USERS_ENDPOINT + "/"));
    }

    @Step("Adding new role = {0} to the user with Id = {1} step ...")
    public ValidatableResponse saveRole(Role role, String userId) {
        return validateResponse(request
                .body(role)
                .contentType(ContentType.JSON)
                .post(USERS_ENDPOINT + "/" + userId + "/set/role"));
    }

    @Step("Adding user`s image with id = {1} step...")
    public ValidatableResponse addUserImage(File image, String id) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .post(USERS_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Adding user`s image-url = {0} with id = {1} step...")
    public ValidatableResponse addUserImageUrl(String url, String id) {
        return validateResponse(request.post(USERS_ENDPOINT + "/" + id + "/image-url?url=" + url));
    }

    @Step("Editing user by id = {0} to new User = {1} step ...")
    public ValidatableResponse editUserById(String id, User newUser) {
        return validateResponse(request
                .body(newUser)
                .contentType(ContentType.JSON)
                .put(USERS_ENDPOINT + "/" + id));
    }

    @Step("Updating user`s image with id = {1} step...")
    public ValidatableResponse editUserImage(File image, String id) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .put(USERS_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Updating user`s image-url = {0} with id = {1} step...")
    public ValidatableResponse editUserImageUrl(String url, String id) {
        return validateResponse(request.put(USERS_ENDPOINT + "/" + id + "/url-image?url=" + url));
    }

    @Step("Deleting user by id = {0} step ...")
    public ValidatableResponse deleteUserById(String id) {
        return validateResponse(request.delete(USERS_ENDPOINT + "/" + id));
    }

    @Step("Deleting user's image with id = {0} step...")
    public ValidatableResponse deleteUsersImage(String id) {
        return validateResponse(request.delete(USERS_ENDPOINT + "/" + id + "/image"));
    }

    @Step("Deleting user's role = {1} with id = {0} step...")
    public ValidatableResponse deleteUsersRole(String userId, Role role) {
        return validateResponse(request
                .body(role)
                .contentType(ContentType.JSON)
                .delete(USERS_ENDPOINT + "/" + userId + "/delete/role"));
    }

}
