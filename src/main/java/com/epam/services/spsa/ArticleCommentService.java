package com.epam.services.spsa;

import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import java.io.File;

import static com.epam.utils.Constants.BLOG_ENDPOINT;

public class ArticleCommentService extends Service {

    @Step("Deleting comment with id = {0} step...")
    public ValidatableResponse deleteComment(String id) {
        return validateResponse(request.delete(BLOG_ENDPOINT + "/comments/" + id));
    }

    @Step("Getting all comments step...")
    public ValidatableResponse getAllComments() {
        return validateResponse(request.get(BLOG_ENDPOINT + "/comments"));
    }

    @Step("Getting comment by id = {0} step...")
    public ValidatableResponse getCommentById(String id) {
        return validateResponse(request.get(BLOG_ENDPOINT + "/comments/" + id));
    }

    @Step("Getting all comments by articleId = {0} and type = {1} step...")
    public ValidatableResponse getCommentsByArticleID(String articleId, String type) {
        return validateResponse(request.get(BLOG_ENDPOINT + articleId + "/comments?type=" + type));
    }

    @Step("Incrementing the rating of the comment with id = {0} step...")
    public ValidatableResponse incrementRatingOfComment(String id) {
        return validateResponse(request.patch(BLOG_ENDPOINT + "/comments/" + id + "/inc"));
    }

    @Step("Decrementing the rating of the comment with id = {0} step...")
    public ValidatableResponse decrementRatingOfComment(String id) {
        return validateResponse(request.patch(BLOG_ENDPOINT + "/comments/" + id + "/dec"));
    }

    @Step("Adding new reply comment with image, commentId = {0}, userId = {1}, comment = {3}, step...")
    public ValidatableResponse addNewReplyCommentWithImage(String commentId, String userId, File image, String comment) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .post(BLOG_ENDPOINT + "/reply-comments-img/" + commentId + "/user/"
                        + userId + "?commentDto=" + comment));
    }

    @Step("Adding new reply comment with commentId = {0}, userId = {1}, comment = {2}, step...")
    public ValidatableResponse addNewReplyComment(String commentId, String userId, String comment) {
        return validateResponse(request
                .post(BLOG_ENDPOINT + "/reply-comments/" + commentId + "/user/" + userId + "?commentDto=" + comment));
    }

    @Step("Adding new comment with image, blogId = {0}, userId = {1}, comment = {3}, step...")
    public ValidatableResponse addNewCommentWithImage(String blogId, String userId, File image, String comment) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .post(BLOG_ENDPOINT + "/" + blogId + "/comments-img/user/" + userId + "?commentDto=" + comment));
    }

    @Step("Adding new comment with blogId = {0}, userId = {1}, comment = {2}, step...")
    public ValidatableResponse addNewComment(String blogId, String userId, String comment) {
        return validateResponse(request
                .post(BLOG_ENDPOINT + "/" + blogId + "/comments/user/" + userId + "?commentDto=" + comment));
    }

    @Step("Updating comment with image, commentId = {0}, comment = {2}, step...")
    public ValidatableResponse editCommentWithImage(String commentId, File image, String comment) {
        return validateResponse(request
                .multiPart("file", image, "image/jpeg")
                .when()
                .put(BLOG_ENDPOINT + "/comments-img/" + commentId + "?commentDto=" + comment));
    }

    @Step("Updating comment with commentId = {0}, comment = {1}, step...")
    public ValidatableResponse editComment(String commentId, String comment) {
        return validateResponse(request
                .put(BLOG_ENDPOINT + "/comments/" + commentId + "?commentDto=" + comment));
    }
}
