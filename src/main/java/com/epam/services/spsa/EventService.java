package com.epam.services.spsa;

import com.epam.model.Event;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.EVENT_ENDPOINT;

public class EventService extends Service {

    @Step("Getting event by eventId = {0} step...")
    public ValidatableResponse getEventByEventId(String id) {
        return validateResponse(request.get(EVENT_ENDPOINT + "/" + id));
    }

    @Step("Getting event by user_id = {0} step...")
    public ValidatableResponse getEventByUserId(String id) {
        return validateResponse(request.get(EVENT_ENDPOINT + "/user/" + id));
    }

    @Step("Getting all event step...")
    public ValidatableResponse getAllEvents() {
        return validateResponse(request.get(EVENT_ENDPOINT + "/"));
    }

    @Step("Adding new event = {0} step...")
    public ValidatableResponse addEvent(Event event) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(event)
                .post(EVENT_ENDPOINT + "/"));
    }

    @Step("Updating event = {0} with id = {1} step...")
    public ValidatableResponse editEvent(Event event, String id) {
        return validateResponse(request
                .contentType(ContentType.JSON)
                .body(event)
                .put(EVENT_ENDPOINT + "/" + id));
    }

    @Step("Deleting event with id = {0} step...")
    public ValidatableResponse deleteEvent(String id, String token) {
        return validateResponse(request
                .cookie("AJWT", token)
                .delete(EVENT_ENDPOINT + "/" + id));
    }
}
