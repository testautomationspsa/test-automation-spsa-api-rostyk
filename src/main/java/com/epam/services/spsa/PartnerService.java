package com.epam.services.spsa;

import com.epam.model.Criteria;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.USERS_ENDPOINT;

public class PartnerService extends Service {

    @Step("Getting all criteria step...")
    public ValidatableResponse getAllCriteria() {
        return validateResponse(request.get(USERS_ENDPOINT + "/criteria"));
    }

    @Step("Getting all criteria by users id = {0} step...")
    public ValidatableResponse getAllCriteriaByUserId(String id) {
        return validateResponse(request.get(USERS_ENDPOINT + "/" + id + "/criteria"));
    }

    @Step("Saving new criteria = {1} by users id = {0} step...")
    public ValidatableResponse saveCriteria(String usersId, Criteria criteria) {
        return validateResponse(request
                .body(criteria)
                .contentType("application/json")
                .post(USERS_ENDPOINT + "/" + usersId + "/criteria"));
    }

    @Step("Delete criteria with id = {1} by users id = {0} step...")
    public ValidatableResponse deleteCriteria(String usersId, String criteriaId) {
        return validateResponse(request.delete(USERS_ENDPOINT + "/" + usersId + "/criteria/" + criteriaId));
    }

    @Step("Getting list of partners with criteria = {1}, pageNumber = {2} and pageSize = {3} by users id = {0} step...")
    public ValidatableResponse getPartners(String usersId, Criteria criteria, String pageNumber, String pageSIze) {
        return validateResponse(request
                .body(criteria)
                .contentType("application/json")
                .post(USERS_ENDPOINT + "/" + usersId + "/partners" +"?pageNumber="+pageNumber+"&pageSize="+pageSIze));
    }
}
