package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Data
public class Event {
    private int id;
    private String date;
    private String time;
    private String description;
    private int location_id;
    private int owner_id;
    private List<Integer> userIdList = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return location_id == event.location_id &&
                owner_id == event.owner_id &&
                Objects.equals(date, event.date) &&
                Objects.equals(time, event.time) &&
                Objects.equals(description, event.description) &&
                verifyLists(userIdList, event.userIdList);
    }

    private boolean verifyLists(List<Integer> listA, List<Integer> listB){
        AtomicBoolean result = new AtomicBoolean(true);
        listA.forEach(el -> result.set(listB.contains(el)));
        return result.get();
    }
}
