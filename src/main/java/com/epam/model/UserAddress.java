package com.epam.model;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserAddress extends Address{
    private int userId;
}

