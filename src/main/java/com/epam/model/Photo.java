package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Photo {
    private int id;
    private String url;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return url.equals(photo.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }
}
