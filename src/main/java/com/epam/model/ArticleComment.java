package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class ArticleComment {
    private int id;
    private String creationDate;
    private String content;
    private int rating;
    private String image;
    private CommentUser user;
    private List<ArticleComment> comments = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleComment that = (ArticleComment) o;
        return rating == that.rating &&
                Objects.equals(content, that.content) &&
                Objects.equals(image, that.image) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, content, rating, image, user);
    }
}
