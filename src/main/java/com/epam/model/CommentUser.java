package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class CommentUser {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String photo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentUser that = (CommentUser) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(email, that.email) &&
                Objects.equals(photo, that.photo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, email, photo);
    }
}
