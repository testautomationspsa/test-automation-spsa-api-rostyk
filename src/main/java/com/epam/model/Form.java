package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Form {
    private int id;
    private Address address;
    private String locationName;
    private Role role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return Objects.equals(address, form.address) &&
                Objects.equals(locationName, form.locationName) &&
                Objects.equals(role, form.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, locationName, role);
    }
}
