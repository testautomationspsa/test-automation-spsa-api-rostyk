package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PartnerShell {
    private List<CriteriaUser> entities = new ArrayList<>();
    private int quantity;
    private int entitiesLeft;
}
