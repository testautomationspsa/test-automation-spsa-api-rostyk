package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CriteriaUser {
    private String city;
    private String email;
    private String gender;
    private String photo;
    private int id;
    private String name;
    private Criteria criteriaDto;
}
