package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class LocationType {
    private String placing;
    private String name;
    private int id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationType that = (LocationType) o;
        return placing.toLowerCase().equals(that.placing.toLowerCase()) &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(placing, name);
    }
}
