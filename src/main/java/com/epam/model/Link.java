package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Link {
    private int id;
    private String type;
    private String url;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return Objects.equals(type.toLowerCase(), link.type.toLowerCase()) &&
                Objects.equals(url, link.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, url);
    }
}
