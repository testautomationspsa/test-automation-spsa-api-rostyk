package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class LocationSchedule {
    private int id;
    private String day;
    private String startTime;
    private String endTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationSchedule that = (LocationSchedule) o;
        return day.equals(that.day) &&
                startTime.equals(that.startTime) &&
                endTime.equals(that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, startTime, endTime);
    }
}
