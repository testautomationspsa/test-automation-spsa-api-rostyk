package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Criteria {
    private int id;
    private String activityTime;
    private String gender;
    private String maturity;
    private double runningDistance;
    private SportType sportType;
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Criteria criteria = (Criteria) o;
        return Double.compare(criteria.runningDistance, runningDistance) == 0 &&
                Objects.equals(activityTime.toLowerCase(), criteria.activityTime.toLowerCase()) &&
                Objects.equals(gender.toLowerCase(), criteria.gender.toLowerCase()) &&
                Objects.equals(maturity.toLowerCase(), criteria.maturity.toLowerCase()) &&
                Objects.equals(sportType, criteria.sportType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityTime, gender, maturity, runningDistance, sportType, user);
    }
}
