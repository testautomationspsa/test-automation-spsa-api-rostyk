package com.epam.model.enums;

public enum LinkType {
    YOUTUBE("Youtube"),
    INSTAGRAM("Instagram"),
    FACEBOOK("Facebook");

    private String name;

    LinkType(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
