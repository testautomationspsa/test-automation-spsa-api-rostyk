package com.epam.model.enums;

public enum Maturity {

    BEGINNER("Beginner"),
    MIDDLE("Middle"),
    PRO("PRO");

    private String name;

    Maturity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
