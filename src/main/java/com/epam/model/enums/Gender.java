package com.epam.model.enums;

public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    BOTH("Both");

    private String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
