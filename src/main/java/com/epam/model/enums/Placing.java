package com.epam.model.enums;

public enum Placing {
    INDOOR("Indoor"),
    OUTDOOR("Outdoor"),
    ANY("Any");

    private String name;

    Placing(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
