package com.epam.model.enums;

public enum RoleEnum {
    USER("User"),
    COACH("Coach"),
    SUPER_ADMIN("Super_admin"),
    VENUE_ADMIN("Venue_admin");

    private String name;

    RoleEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
