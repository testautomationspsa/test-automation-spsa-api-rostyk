package com.epam.model.enums;

public enum ActivityTime {
    MORNING("Morning"),
    NOON("Noon"),
    EVENING("Morning"),
    ALL("All");

    private String name;

    ActivityTime(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
