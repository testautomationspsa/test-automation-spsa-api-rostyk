package com.epam.model.enums;

public enum SportTypeEnum {
    RUNNING("Running"),
    SWIMMING("Swimming"),
    FOOTBALL("Football"),
    YOGA("Yoga");

    private String name;

    SportTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
