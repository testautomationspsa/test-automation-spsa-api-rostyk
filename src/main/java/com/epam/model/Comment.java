package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Comment {
    private int id;
    private int userId;
    private User user;
    private int coachId;
    private String creationDate;
    private String comment;
    private int rating;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return rating == comment1.rating &&
                comment.equals(comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comment, rating);
    }
}
