package com.epam.model;

import lombok.Data;

import java.util.*;

@Data
public class Location {
    private int id;
    private LocationAddress address;
    private String webSite;
    private String locationWebSite;
    private String phoneNumber;
    private String name;
    private Integer adminId;
    private LocationType locationType;
    private Set<SportType> sportTypes = new HashSet<>();
    private List<LocationSchedule> locationSchedule = new ArrayList<>();
    private List<Coach> coaches = new ArrayList<>();
    private List<Photo> photos = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return address.equals(location.address) &&
                name.equals(location.name) &&
                Objects.equals(webSite, location.webSite) &&
                Objects.equals(phoneNumber, location.phoneNumber) &&
                locationType.equals(location.locationType) &&
                sportTypes.equals(location.sportTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, name, locationType, sportTypes);
    }
}
