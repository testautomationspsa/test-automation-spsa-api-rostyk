package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String phoneNumber;
    private String dateOfBirth;
    private UserAddress address;
    private String lastSeen;
    private boolean online;
    private String photo;
    private String gender;
    private String creationDate;
    private boolean hasChildren;
    private String password;
    private List<Role> roles = new ArrayList<>();
    private List<Chat> chats = new ArrayList<>();
    private List<Event> events = new ArrayList<>();
    private List<Event> ownEvents = new ArrayList<>();
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return hasChildren == user.isHasChildren() &&
                Objects.equals(name, user.getName()) &&
                Objects.equals(surname, user.getSurname()) &&
                Objects.equals(email, user.getEmail()) &&
                Objects.equals(dateOfBirth, user.getDateOfBirth()) &&
                Objects.equals(phoneNumber, user.getPhoneNumber()) &&
                Objects.equals(gender.toLowerCase(),user.getGender().toLowerCase());
    }

}
