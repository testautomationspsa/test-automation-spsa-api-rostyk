package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Coach {
    private int id;
    private int userId;
    private int rating;
    private User user;
    private boolean workWithChildren;
    private List<SportType> sportTypes = new ArrayList<>();
    private List<Link> links = new ArrayList<>();
    private List<Comment> comments = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coach coach = (Coach) o;
        return rating == coach.rating &&
                userId == coach.userId &&
                workWithChildren == coach.workWithChildren &&
                Objects.equals(sportTypes, coach.sportTypes) &&
                Objects.equals(links, coach.links) &&
                Objects.equals(comments, coach.comments);
    }
}
