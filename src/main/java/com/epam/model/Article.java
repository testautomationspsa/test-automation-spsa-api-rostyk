package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    private int id;
    private String authorName;
    private String authorSurname;
    private String content;
    private String creationDate;
    private String pictureUrl;
    private String description;
    private String topic;
    private List<Tag> tags = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return authorName.equals(article.authorName) &&
                authorSurname.equals(article.authorSurname) &&
                content.equals(article.content) &&
                creationDate.equals(article.creationDate) &&
                topic.equals(article.topic) &&
                tags.equals(article.tags);
    }
}
