package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class LocationCriteria {
    private int id;
    private double latitude;
    private double longitude;
    private String placing;
    private List<SportType> sportTypes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationCriteria that = (LocationCriteria) o;
        return Double.compare(that.latitude, latitude) == 0 &&
                Double.compare(that.longitude, longitude) == 0 &&
                placing.equals(that.placing) &&
                Objects.equals(sportTypes, that.sportTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude, placing, sportTypes);
    }
}
