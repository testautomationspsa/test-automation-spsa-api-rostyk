package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class SportType {
    private int id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SportType sportType = (SportType) o;
        return name.equals(sportType.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
