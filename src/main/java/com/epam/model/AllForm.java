package com.epam.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AllForm {
    private List<Form> entities = new ArrayList<>();
    private int quantity;
    private int entitiesLeft;
}
