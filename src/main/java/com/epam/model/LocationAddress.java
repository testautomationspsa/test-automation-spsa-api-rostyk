package com.epam.model;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LocationAddress extends Address{
    private int locationId;
}


