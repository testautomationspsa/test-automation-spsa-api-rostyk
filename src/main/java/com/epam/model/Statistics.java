package com.epam.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Statistics {
    private int coachId;
    private int locationId;
    private int userId;
    private String resultH;
    private double resultKm;
    private SportType sportType;
    private String insertionDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statistics that = (Statistics) o;
        return Double.compare(that.resultKm, resultKm) == 0 &&
                Objects.equals(resultH, that.resultH) &&
                Objects.equals(sportType, that.sportType);
    }
}
